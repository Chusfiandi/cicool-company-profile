-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Sep 2020 pada 09.47
-- Versi server: 10.4.14-MariaDB
-- Versi PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ipm`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_groups`
--

CREATE TABLE `aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Superadmin Group'),
(2, 'Public', 'Public Group'),
(3, 'Default', 'Default Access Group'),
(4, 'Member', 'Member Access Group');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_group_to_group`
--

CREATE TABLE `aauth_group_to_group` (
  `group_id` int(11) UNSIGNED NOT NULL,
  `subgroup_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_login_attempts`
--

CREATE TABLE `aauth_login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_perms`
--

CREATE TABLE `aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aauth_perms`
--

INSERT INTO `aauth_perms` (`id`, `name`, `definition`) VALUES
(1, 'menu_dashboard', NULL),
(2, 'menu_crud_builder', NULL),
(3, 'menu_api_builder', NULL),
(4, 'menu_page_builder', NULL),
(5, 'menu_form_builder', NULL),
(6, 'menu_menu', NULL),
(7, 'menu_auth', NULL),
(8, 'menu_user', NULL),
(9, 'menu_group', NULL),
(10, 'menu_access', NULL),
(11, 'menu_permission', NULL),
(12, 'menu_api_documentation', NULL),
(13, 'menu_web_documentation', NULL),
(14, 'menu_settings', NULL),
(15, 'user_list', NULL),
(16, 'user_update_status', NULL),
(17, 'user_export', NULL),
(18, 'user_add', NULL),
(19, 'user_update', NULL),
(20, 'user_update_profile', NULL),
(21, 'user_update_password', NULL),
(22, 'user_profile', NULL),
(23, 'user_view', NULL),
(24, 'user_delete', NULL),
(25, 'blog_list', NULL),
(26, 'blog_export', NULL),
(27, 'blog_add', NULL),
(28, 'blog_update', NULL),
(29, 'blog_view', NULL),
(30, 'blog_delete', NULL),
(31, 'form_list', NULL),
(32, 'form_export', NULL),
(33, 'form_add', NULL),
(34, 'form_update', NULL),
(35, 'form_view', NULL),
(36, 'form_manage', NULL),
(37, 'form_delete', NULL),
(38, 'crud_list', NULL),
(39, 'crud_export', NULL),
(40, 'crud_add', NULL),
(41, 'crud_update', NULL),
(42, 'crud_view', NULL),
(43, 'crud_delete', NULL),
(44, 'rest_list', NULL),
(45, 'rest_export', NULL),
(46, 'rest_add', NULL),
(47, 'rest_update', NULL),
(48, 'rest_view', NULL),
(49, 'rest_delete', NULL),
(50, 'group_list', NULL),
(51, 'group_export', NULL),
(52, 'group_add', NULL),
(53, 'group_update', NULL),
(54, 'group_view', NULL),
(55, 'group_delete', NULL),
(56, 'permission_list', NULL),
(57, 'permission_export', NULL),
(58, 'permission_add', NULL),
(59, 'permission_update', NULL),
(60, 'permission_view', NULL),
(61, 'permission_delete', NULL),
(62, 'access_list', NULL),
(63, 'access_add', NULL),
(64, 'access_update', NULL),
(65, 'menu_list', NULL),
(66, 'menu_add', NULL),
(67, 'menu_update', NULL),
(68, 'menu_delete', NULL),
(69, 'menu_save_ordering', NULL),
(70, 'menu_type_add', NULL),
(71, 'page_list', NULL),
(72, 'page_export', NULL),
(73, 'page_add', NULL),
(74, 'page_update', NULL),
(75, 'page_view', NULL),
(76, 'page_delete', NULL),
(77, 'blog_list', NULL),
(78, 'blog_export', NULL),
(79, 'blog_add', NULL),
(80, 'blog_update', NULL),
(81, 'blog_view', NULL),
(82, 'blog_delete', NULL),
(83, 'setting', NULL),
(84, 'setting_update', NULL),
(85, 'dashboard', NULL),
(86, 'extension_list', NULL),
(87, 'extension_activate', NULL),
(88, 'extension_deactivate', NULL),
(89, 'about_add', ''),
(90, 'about_update', ''),
(91, 'about_view', ''),
(92, 'about_delete', ''),
(93, 'about_list', ''),
(94, 'menu_about', ''),
(95, 'form_about_add', ''),
(96, 'form_about_update', ''),
(97, 'form_about_view', ''),
(98, 'form_about_delete', ''),
(99, 'about_export', ''),
(105, 'menu_products', ''),
(106, 'menu_blog', ''),
(107, 'service_add', ''),
(108, 'service_update', ''),
(109, 'service_view', ''),
(110, 'service_delete', ''),
(111, 'service_list', ''),
(112, 'menu_service', ''),
(113, 'form_contact_add', ''),
(114, 'form_contact_update', ''),
(115, 'form_contact_view', ''),
(116, 'form_contact_delete', ''),
(117, 'form_about_list', ''),
(118, 'form_contact_list', ''),
(119, 'contact_add', ''),
(120, 'contact_update', ''),
(121, 'contact_view', ''),
(122, 'contact_delete', ''),
(123, 'contact_list', ''),
(129, 'career_add', ''),
(130, 'career_update', ''),
(131, 'career_view', ''),
(132, 'career_delete', ''),
(133, 'career_list', ''),
(134, 'menu_contact', ''),
(135, 'products_add', ''),
(136, 'products_update', ''),
(137, 'products_view', ''),
(138, 'products_delete', ''),
(139, 'products_list', ''),
(140, 'contact_index', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_perm_to_group`
--

CREATE TABLE `aauth_perm_to_group` (
  `perm_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aauth_perm_to_group`
--

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES
(134, 1),
(134, 1),
(105, 1),
(94, 1),
(112, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_perm_to_user`
--

CREATE TABLE `aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_pms`
--

CREATE TABLE `aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(225) NOT NULL,
  `message` text DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT NULL,
  `pm_deleted_receiver` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_user`
--

CREATE TABLE `aauth_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_users`
--

CREATE TABLE `aauth_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `oauth_uid` text DEFAULT NULL,
  `oauth_provider` varchar(100) DEFAULT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `avatar` text NOT NULL,
  `banned` tinyint(1) DEFAULT 0,
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text DEFAULT NULL,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text DEFAULT NULL,
  `verification_code` text DEFAULT NULL,
  `top_secret` varchar(16) DEFAULT NULL,
  `ip_address` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `oauth_uid`, `oauth_provider`, `pass`, `username`, `full_name`, `avatar`, `banned`, `last_login`, `last_activity`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `top_secret`, `ip_address`) VALUES
(1, 'ipm@gmail.com', NULL, NULL, 'a4c3c4664493eb0ebc347373e3768ed3779ac743c281ce1eb2b4d23e21e49e93', 'ipm', 'ipm', '', 0, '2020-09-07 12:58:28', '2020-09-07 12:58:28', '2020-08-28 04:57:13', NULL, NULL, NULL, NULL, NULL, '::1'),
(2, 'chus@gmail.com', NULL, NULL, 'd1f3550d48629e999835a888a3f0124b7dba09e1de13cd426083c7edcdd55b52', 'chus', 'chus', '', 0, NULL, NULL, '2020-09-07 09:46:16', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_user_to_group`
--

CREATE TABLE `aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(1, 1),
(1, 3),
(2, 3),
(2, 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aauth_user_variables`
--

CREATE TABLE `aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `about` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `about`
--

INSERT INTO `about` (`id`, `about`, `visi`, `misi`) VALUES
(1, 'PT. Intikemas Putra Makmur was established in July 2014. The company is continously growing and is in the process of an ongoing expansion. We will increase the production capacity with add a few new machines such as Printing Machine, Standing Pouch, Zipper and Center Seal Bag Making Machines which targetted to be completed in 2018.<br />\r\n<br />\r\nThe company is run by a management team with more than 15 years of experience in the felxible packaging industry and supported by professional engineers and scientists.<br />\r\n<br />\r\nAs demonstrated in the company obtaining the ISO 22000 Food safety Management System, we are trully commited to food safety policy program, at the end of 2017, we will upgrade ISO 22000 become FSSC 22000.<br />\r\n<br />\r\nPT. Intikemas Putra Makmur also certified Halal Management System by MUI since October 2017, and also plan to implement environment management system ISO 14001 and Safety Management System OHSAS 18001 during 2017.', 'To be recognized as a responsible leader in providing flexible packaging in the Asia-Pacific region', '<ol>\r\n	<li>To build customer relationships on honesty and integrity</li>\r\n	<li>To ensure a safe and healthy working conditions for employees.</li>\r\n	<li>To produce high and safe quality packaging solutions</li>\r\n	<li>To innovate in product development</li>\r\n</ol>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` text NOT NULL,
  `tags` text NOT NULL,
  `category` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `author` varchar(100) NOT NULL,
  `viewers` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `slug`, `content`, `image`, `tags`, `category`, `status`, `author`, `viewers`, `created_at`, `updated_at`) VALUES
(1, 'Hello Wellcome To Cicool Builder', 'Hello-Wellcome-To-Ciool-Builder', 'greetings from our team I hope to be happy!', '20200904132843-2020-09-04blog132838.jpg', 'greetings', '1', 'publish', 'ipm', 5, '2020-08-28 04:57:13', '2020-09-04 13:28:43'),
(3, 'blog 1', 'blog-1', 'blog1', '20200904120720-2020-09-04blog120714.jpg', 'holiday', '2', 'publish', 'ipm', 0, '2020-09-01 19:03:19', '2020-09-04 12:07:20'),
(4, 'blog1', 'blog1', 'blog1\r\n<ol>\r\n	<li>ko</li>\r\n	<li>4</li>\r\n	<li>6</li>\r\n</ol>', '20200901190801-2020-09-01blog190747.jpg', 'holiday', '1', 'publish', 'ipm', 1, '2020-09-01 19:08:01', '0000-00-00 00:00:00'),
(5, 'tes1', 'tes1', 'tes1', '20200903184030-2020-09-03blog184029.jpg', 'islami lifestyle', '2', 'publish', 'ipm', 0, '2020-09-03 18:39:48', '2020-09-03 18:40:30'),
(6, 'tes2', 'tes2', 'tes2', '20200903184013-2020-09-03blog184012.jpg', 'islami lifestyle', '1', 'publish', 'ipm', 0, '2020-09-03 18:40:13', '0000-00-00 00:00:00'),
(7, 'tes4', 'tes4', 'tes4', '20200903184113-2020-09-03blog184112.jpg', 'holiday', '1', 'publish', 'ipm', 0, '2020-09-03 18:40:53', '2020-09-03 18:41:13'),
(8, 'tyss', 'tyss', 'hgsg', '20200904120648-2020-09-04blog120647.jpg', 'holiday', '1', 'publish', 'ipm', 0, '2020-09-03 18:42:47', '2020-09-04 12:06:48'),
(9, 'teyajjaj', 'teyajjaj', 'hekkka', '20200903184312-2020-09-03blog184311.png', 'holiday', '2', 'publish', 'ipm', 0, '2020-09-03 18:43:12', '0000-00-00 00:00:00'),
(10, 'sajaa', 'sajaa', 'dasad', '20200903184341-2020-09-03blog184341.jpg', 'holiday', '2', 'publish', 'ipm', 0, '2020-09-03 18:43:41', '0000-00-00 00:00:00'),
(11, 'shjakj', 'shjakj', 'sajjaa', '20200903184516-2020-09-03blog184514.jpg', 'holiday', '1', 'publish', 'ipm', 0, '2020-09-03 18:45:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog_category`
--

CREATE TABLE `blog_category` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `category_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `blog_category`
--

INSERT INTO `blog_category` (`category_id`, `category_name`, `category_desc`) VALUES
(1, 'Technology', ''),
(2, 'Lifestyle', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `captcha`
--

CREATE TABLE `captcha` (
  `captcha_id` int(11) UNSIGNED NOT NULL,
  `captcha_time` int(10) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `word` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(292, 1599457547, '::1', 'THS1'),
(293, 1599457578, '::1', 'GQLW'),
(294, 1599457634, '::1', 'F2Y8'),
(295, 1599457649, '::1', 'M39S'),
(296, 1599457655, '::1', 'K9YQ'),
(297, 1599457661, '::1', 'ARXJ'),
(298, 1599457721, '::1', 'D6VP'),
(299, 1599457734, '::1', 'QVRL'),
(300, 1599457751, '::1', '41DP'),
(301, 1599457756, '::1', 'K536'),
(302, 1599457777, '::1', 'WMMO'),
(303, 1599457782, '::1', 'V4XS'),
(304, 1599457787, '::1', 'YXJS'),
(305, 1599457796, '::1', 'R8FK'),
(306, 1599457826, '::1', '8VAQ'),
(307, 1599457837, '::1', 'SMBI'),
(308, 1599457956, '::1', 'BIYM'),
(309, 1599457968, '::1', '928X'),
(310, 1599457974, '::1', 'DX01'),
(311, 1599457992, '::1', '4B5N'),
(312, 1599458004, '::1', '5BLK'),
(313, 1599458069, '::1', 'I4C6'),
(314, 1599458099, '::1', 'XBOD'),
(315, 1599459137, '::1', 'MEH4'),
(316, 1599459145, '::1', 'AU1Z'),
(317, 1599459526, '::1', 'EDCP'),
(318, 1599459536, '::1', 'QY0Z'),
(319, 1599459591, '::1', '36ML'),
(320, 1599459640, '::1', 'TD9T'),
(321, 1599459687, '::1', 'UN2V'),
(322, 1599459843, '::1', 'OH00'),
(323, 1599459884, '::1', '6CVS'),
(324, 1599461205, '::1', 'UR8B'),
(325, 1599461219, '::1', 'ZFIQ'),
(326, 1599462434, '::1', 'QMJY'),
(330, 1599462460, '::1', 'T2DK'),
(333, 1599464184, '::1', 'HVRW'),
(334, 1599464197, '::1', 'TYZ3'),
(335, 1599464263, '::1', 'QSTJ'),
(337, 1599464433, '::1', 'A11G'),
(338, 1599464659, '::1', 'RE6H'),
(341, 1599464666, '::1', '4BPZ');

-- --------------------------------------------------------

--
-- Struktur dari tabel `career`
--

CREATE TABLE `career` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `career`
--

INSERT INTO `career` (`id`, `title`, `content`) VALUES
(1, 'Job Vacancy', '<label id=\"judul\">BAGIAN:</label>\r\n<p>1. Operator Bag Making<br />\r\n2. Operator Printing<br />\r\n3. Operator Laminasi (Dry/ Extrusion)<br />\r\n4. Operator Sliting<br />\r\n5. Warehouse (Raw Material, Tinta, Cylinder, Finish Good, Sparepart)<br />\r\n6. Quality Control<br />\r\n7. PPIC<br />\r\n8. Pekerjaan Civil<br />\r\n9. Engineering<br />\r\n10. Teknik Otomotif</p>\r\n<label id=\"judul\">KUALIFIKASI:</label>\r\n\r\n<p>1. Pria<br />\r\n2. Usia Maksimal 35 Tahun<br />\r\n3. Pengalaman Dibidangnya Min. 2 Tahun<br />\r\n4. Pendidikan Terakhir Minimal Sma<br />\r\n5. Sehat Jasmani Dan Rohani<br />\r\n6. Lulus Tes Tertulis, Psikotes Dan Mcu</p>\r\n<label id=\"judul\">PERSYARATAN ADMINISTRASI</label>\r\n\r\n<p>1. Surat Lamaran Kerja<br />\r\n2. Daftar Riwayat Hidup<br />\r\n3. Surat Pengalaman Kerja<br />\r\n4. Fotocopy Ijazah Dan Transkrip Nilai (Legalisir)<br />\r\n5. Fotocopy Ktp Dan Kk<br />\r\n6. Fotocopy Skck Yang Masih Berlaku<br />\r\n7. Surat Keterangan Sehat Dari Dokter (Asli)<br />\r\n8. Pas Foto Berwarna<br />\r\n9. 4 X 6 = 2 Lembar<br />\r\n10. 3 X 4 = 2 Lembar</p>\r\n<br />\r\n<label id=\"judul\">SAP Programmer</label>\r\n\r\n<p>1. Usia 18 sd 35 tahun<br />\r\n2. Pendidikan minimal S1 Ilmu Komputer/Teknik Informatika/Sistem Informasi<br />\r\n3. Lulusan Universitas Terkemuka<br />\r\n4. Keahlian yang di perlukan: Bahasa program ABAP, Crystal Report, Windows &amp; Linux, Visual studio, Net, SQL<br />\r\n5. Pengalaman minimal 2 tahun di bidang SAP<br />\r\n6. Mempunyai ABAP certification (lebih disukai)<br />\r\n7. Sehat Jasmani Dan Rohani<br />\r\n8. Lulus Tes Tertulis, Psikotes Dan MCU</p>\r\n<label id=\"judul\">PERSYARATAN ADMINISTRASI</label>\r\n\r\n<p>1. Surat Lamaran Kerja<br />\r\n2. Daftar Riwayat Hidup<br />\r\n3. Surat Pengalaman Kerja<br />\r\n4. Fotocopy Ijazah Dan Transkrip Nilai (Legalisir)<br />\r\n5. Fotocopy KTPDan KK<br />\r\n6. Fotocopy SKCKYang Masih Berlaku<br />\r\n7. Surat Keterangan Sehat Dari Dokter (Asli)</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `cc_options`
--

CREATE TABLE `cc_options` (
  `id` int(11) UNSIGNED NOT NULL,
  `option_name` varchar(200) NOT NULL,
  `option_value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `cc_options`
--

INSERT INTO `cc_options` (`id`, `option_name`, `option_value`) VALUES
(1, 'enable_crud_builder', NULL),
(2, 'enable_form_builder', NULL),
(3, 'enable_page_builder', NULL),
(4, 'enable_disqus', NULL),
(5, 'disqus_id', ''),
(6, 'site_name', 'PT Intikemas Makmur Putra'),
(7, 'email', 'ipm@gmail.com'),
(8, 'author', ''),
(9, 'site_description', ''),
(10, 'keywords', ''),
(11, 'landing_page_id', 'default'),
(12, 'timezone', 'Australia/Adelaide'),
(13, 'active_theme', 'cicool'),
(14, 'limit_pagination', '10'),
(15, 'google_id', ''),
(16, 'google_secret', ''),
(17, 'logo', '20200828103100-2020-08-28setting103017.jpeg'),
(18, 'enable_api_builder', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `cc_session`
--

CREATE TABLE `cc_session` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) NOT NULL,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `jabatan` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `contact`
--

INSERT INTO `contact` (`id`, `fullname`, `email`, `jabatan`) VALUES
(1, 'Farid', 'fiandichus3@gmail.com', 'Manager'),
(2, 'Chusfiandi', 'fandifarid724@gmail.com', 'Direktur');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud`
--

CREATE TABLE `crud` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `primary_key` varchar(200) NOT NULL,
  `page_read` varchar(20) DEFAULT NULL,
  `page_create` varchar(20) DEFAULT NULL,
  `page_update` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `crud`
--

INSERT INTO `crud` (`id`, `title`, `subject`, `table_name`, `primary_key`, `page_read`, `page_create`, `page_update`) VALUES
(1, 'About', 'About', 'about', 'id', 'yes', NULL, 'yes'),
(3, 'Service', 'Service', 'service', 'serviceID', 'yes', 'yes', 'yes'),
(4, 'Contact', 'Contact', 'contact', 'id', 'yes', 'yes', 'yes'),
(6, 'Career', 'Career', 'career', 'id', 'yes', 'yes', 'yes'),
(7, 'Products', 'Products', 'products', 'productID', 'yes', 'yes', 'yes');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_custom_option`
--

CREATE TABLE `crud_custom_option` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_field`
--

CREATE TABLE `crud_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_id` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `input_type` varchar(200) NOT NULL,
  `show_column` varchar(10) DEFAULT NULL,
  `show_add_form` varchar(10) DEFAULT NULL,
  `show_update_form` varchar(10) DEFAULT NULL,
  `show_detail_page` varchar(10) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `crud_field`
--

INSERT INTO `crud_field` (`id`, `crud_id`, `field_name`, `field_label`, `input_type`, `show_column`, `show_add_form`, `show_update_form`, `show_detail_page`, `sort`, `relation_table`, `relation_value`, `relation_label`) VALUES
(25, 3, 'serviceID', 'serviceID', 'number', '', '', '', 'yes', 1, '', '', ''),
(26, 3, 'service_name', 'service_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(27, 3, 'service_image', 'service_image', 'file', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(28, 3, 'service_description', 'service_description', 'textarea', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(37, 1, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(38, 1, 'about', 'about', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(39, 1, 'visi', 'visi', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(40, 1, 'misi', 'misi', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(45, 5, 'id', 'id', 'number', 'yes', 'yes', 'yes', 'yes', 1, '', '', ''),
(46, 5, 'title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(47, 5, 'content', 'content', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(48, 6, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(49, 6, 'title', 'title', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(50, 6, 'content', 'content', 'editor_wysiwyg', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(51, 4, 'id', 'id', 'number', '', '', '', 'yes', 1, '', '', ''),
(52, 4, 'fullname', 'fullname', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(53, 4, 'email', 'email', 'input', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(54, 4, 'jabatan', 'jabatan', 'input', 'yes', 'yes', 'yes', 'yes', 5, '', '', ''),
(69, 2, 'productID', 'productID', 'input', '', '', '', 'yes', 1, '', '', ''),
(70, 2, 'product_name', 'product_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(71, 2, 'product_image', 'product_image', 'file', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(72, 2, 'product_description', 'product_description', 'textarea', 'yes', 'yes', 'yes', 'yes', 4, '', '', ''),
(73, 7, 'productID', 'productID', 'number', '', '', '', 'yes', 1, '', '', ''),
(74, 7, 'product_name', 'product_name', 'input', 'yes', 'yes', 'yes', 'yes', 2, '', '', ''),
(75, 7, 'product_image', 'product_image', 'file', 'yes', 'yes', 'yes', 'yes', 3, '', '', ''),
(76, 7, 'product_description', 'product_description', 'textarea', 'yes', 'yes', 'yes', 'yes', 4, '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_field_configuration`
--

CREATE TABLE `crud_field_configuration` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `group_config` varchar(200) NOT NULL,
  `config_name` text NOT NULL,
  `config_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_field_validation`
--

CREATE TABLE `crud_field_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `crud_field_id` int(11) NOT NULL,
  `crud_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `crud_field_validation`
--

INSERT INTO `crud_field_validation` (`id`, `crud_field_id`, `crud_id`, `validation_name`, `validation_value`) VALUES
(36, 26, 3, 'required', ''),
(37, 26, 3, 'max_length', '255'),
(38, 27, 3, 'required', ''),
(39, 27, 3, 'allowed_extension', 'jpg,png,jpeg'),
(40, 27, 3, 'max_size', '1500'),
(41, 28, 3, 'required', ''),
(53, 38, 1, 'required', ''),
(54, 38, 1, 'min_length', '20'),
(55, 39, 1, 'required', ''),
(56, 39, 1, 'min_length', '15'),
(57, 40, 1, 'required', ''),
(63, 45, 5, 'required', ''),
(64, 45, 5, 'max_length', '11'),
(65, 46, 5, 'required', ''),
(66, 47, 5, 'required', ''),
(67, 49, 6, 'required', ''),
(68, 50, 6, 'required', ''),
(69, 52, 4, 'required', ''),
(70, 52, 4, 'alpha', ''),
(71, 53, 4, 'required', ''),
(72, 53, 4, 'valid_email', ''),
(73, 54, 4, 'required', ''),
(89, 70, 2, 'required', ''),
(90, 70, 2, 'max_length', '255'),
(91, 71, 2, 'required', ''),
(92, 71, 2, 'max_size', '1000'),
(93, 72, 2, 'required', ''),
(94, 74, 7, 'required', ''),
(95, 75, 7, 'required', ''),
(96, 76, 7, 'required', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_input_chained`
--

CREATE TABLE `crud_input_chained` (
  `id` int(11) UNSIGNED NOT NULL,
  `chain_field` varchar(250) DEFAULT NULL,
  `chain_field_eq` varchar(250) DEFAULT NULL,
  `crud_field_id` int(11) DEFAULT NULL,
  `crud_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_input_type`
--

CREATE TABLE `crud_input_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(200) NOT NULL,
  `relation` varchar(20) NOT NULL,
  `custom_value` int(11) NOT NULL,
  `validation_group` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `crud_input_type`
--

INSERT INTO `crud_input_type` (`id`, `type`, `relation`, `custom_value`, `validation_group`) VALUES
(1, 'input', '0', 0, 'input'),
(2, 'textarea', '0', 0, 'text'),
(3, 'select', '1', 0, 'select'),
(4, 'editor_wysiwyg', '0', 0, 'editor'),
(5, 'password', '0', 0, 'password'),
(6, 'email', '0', 0, 'email'),
(7, 'address_map', '0', 0, 'address_map'),
(8, 'file', '0', 0, 'file'),
(9, 'file_multiple', '0', 0, 'file_multiple'),
(10, 'datetime', '0', 0, 'datetime'),
(11, 'date', '0', 0, 'date'),
(12, 'timestamp', '0', 0, 'timestamp'),
(13, 'number', '0', 0, 'number'),
(14, 'yes_no', '0', 0, 'yes_no'),
(15, 'time', '0', 0, 'time'),
(16, 'year', '0', 0, 'year'),
(17, 'select_multiple', '1', 0, 'select_multiple'),
(18, 'checkboxes', '1', 0, 'checkboxes'),
(19, 'options', '1', 0, 'options'),
(20, 'true_false', '0', 0, 'true_false'),
(21, 'current_user_username', '0', 0, 'user_username'),
(22, 'current_user_id', '0', 0, 'current_user_id'),
(23, 'custom_option', '0', 1, 'custom_option'),
(24, 'custom_checkbox', '0', 1, 'custom_checkbox'),
(25, 'custom_select_multiple', '0', 1, 'custom_select_multiple'),
(26, 'custom_select', '0', 1, 'custom_select'),
(27, 'chained', '1', 0, 'chained');

-- --------------------------------------------------------

--
-- Struktur dari tabel `crud_input_validation`
--

CREATE TABLE `crud_input_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `validation` varchar(200) NOT NULL,
  `input_able` varchar(20) NOT NULL,
  `group_input` text NOT NULL,
  `input_placeholder` text NOT NULL,
  `call_back` varchar(10) NOT NULL,
  `input_validation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `crud_input_validation`
--

INSERT INTO `crud_input_validation` (`id`, `validation`, `input_able`, `group_input`, `input_placeholder`, `call_back`, `input_validation`) VALUES
(1, 'required', 'no', 'input, file, number, text, datetime, select, password, email, editor, date, yes_no, time, year, select_multiple, options, checkboxes, true_false, address_map, custom_option, custom_checkbox, custom_select_multiple, custom_select, file_multiple, chained', '', '', ''),
(2, 'max_length', 'yes', 'input, number, text, select, password, email, editor, yes_no, time, year, select_multiple, options, checkboxes, address_map', '', '', 'numeric'),
(3, 'min_length', 'yes', 'input, number, text, select, password, email, editor, time, year, select_multiple, address_map', '', '', 'numeric'),
(4, 'valid_email', 'no', 'input, email', '', '', ''),
(5, 'valid_emails', 'no', 'input, email', '', '', ''),
(6, 'regex', 'yes', 'input, number, text, datetime, select, password, email, editor, date, yes_no, time, year, select_multiple, options, checkboxes', '', 'yes', 'callback_valid_regex'),
(7, 'decimal', 'no', 'input, number, text, select', '', '', ''),
(8, 'allowed_extension', 'yes', 'file, file_multiple', 'ex : jpg,png,..', '', 'callback_valid_extension_list'),
(9, 'max_width', 'yes', 'file, file_multiple', '', '', 'numeric'),
(10, 'max_height', 'yes', 'file, file_multiple', '', '', 'numeric'),
(11, 'max_size', 'yes', 'file, file_multiple', '... kb', '', 'numeric'),
(12, 'max_item', 'yes', 'file_multiple', '', '', 'numeric'),
(13, 'valid_url', 'no', 'input, text', '', '', ''),
(14, 'alpha', 'no', 'input, text, select, password, editor, yes_no', '', '', ''),
(15, 'alpha_numeric', 'no', 'input, number, text, select, password, editor', '', '', ''),
(16, 'alpha_numeric_spaces', 'no', 'input, number, text,select, password, editor', '', '', ''),
(17, 'valid_number', 'no', 'input, number, text, password, editor, true_false', '', 'yes', ''),
(18, 'valid_datetime', 'no', 'input, datetime, text', '', 'yes', ''),
(19, 'valid_date', 'no', 'input, datetime, date, text', '', 'yes', ''),
(20, 'valid_max_selected_option', 'yes', 'select_multiple, custom_select_multiple, custom_checkbox, checkboxes', '', 'yes', 'numeric'),
(21, 'valid_min_selected_option', 'yes', 'select_multiple, custom_select_multiple, custom_checkbox, checkboxes', '', 'yes', 'numeric'),
(22, 'valid_alpha_numeric_spaces_underscores', 'no', 'input, text,select, password, editor', '', 'yes', ''),
(23, 'matches', 'yes', 'input, number, text, password, email', 'any field', 'no', 'callback_valid_alpha_numeric_spaces_underscores'),
(24, 'valid_json', 'no', 'input, text, editor', '', 'yes', ' '),
(25, 'valid_url', 'no', 'input, text, editor', '', 'no', ' '),
(26, 'exact_length', 'yes', 'input, text, number', '0 - 99999*', 'no', 'numeric'),
(27, 'alpha_dash', 'no', 'input, text', '', 'no', ''),
(28, 'integer', 'no', 'input, text, number', '', 'no', ''),
(29, 'differs', 'yes', 'input, text, number, email, password, editor, options, select', 'any field', 'no', 'callback_valid_alpha_numeric_spaces_underscores'),
(30, 'is_natural', 'no', 'input, text, number', '', 'no', ''),
(31, 'is_natural_no_zero', 'no', 'input, text, number', '', 'no', ''),
(32, 'less_than', 'yes', 'input, text, number', '', 'no', 'numeric'),
(33, 'less_than_equal_to', 'yes', 'input, text, number', '', 'no', 'numeric'),
(34, 'greater_than', 'yes', 'input, text, number', '', 'no', 'numeric'),
(35, 'greater_than_equal_to', 'yes', 'input, text, number', '', 'no', 'numeric'),
(36, 'in_list', 'yes', 'input, text, number, select, options', '', 'no', 'callback_valid_multiple_value'),
(37, 'valid_ip', 'no', 'input, text', '', 'no', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `form`
--

CREATE TABLE `form` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `form`
--

INSERT INTO `form` (`id`, `title`, `subject`, `table_name`) VALUES
(1, 'About', 'About', 'form_about'),
(2, 'contact', 'contact', 'form_contact');

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_about`
--

CREATE TABLE `form_about` (
  `id` int(11) UNSIGNED NOT NULL,
  `about` text DEFAULT NULL,
  `visi` text DEFAULT NULL,
  `misi` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_contact`
--

CREATE TABLE `form_contact` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `messsage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `form_contact`
--

INSERT INTO `form_contact` (`id`, `name`, `email`, `messsage`) VALUES
(1, 'chusfiandi', 'chusfiandi@gmail.com', 'sdfsfsdf'),
(2, 'sasdaad', 'user@gmail.com', 'aaadsadadada'),
(3, 'chsu', 'scu@sik.cls', 'jshja shhsh');

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_custom_attribute`
--

CREATE TABLE `form_custom_attribute` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `attribute_value` text NOT NULL,
  `attribute_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_custom_option`
--

CREATE TABLE `form_custom_option` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `option_value` text NOT NULL,
  `option_label` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_field`
--

CREATE TABLE `form_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `input_type` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `placeholder` text DEFAULT NULL,
  `auto_generate_help_block` varchar(10) DEFAULT NULL,
  `help_block` text DEFAULT NULL,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `form_field`
--

INSERT INTO `form_field` (`id`, `form_id`, `sort`, `field_name`, `input_type`, `field_label`, `placeholder`, `auto_generate_help_block`, `help_block`, `relation_table`, `relation_value`, `relation_label`) VALUES
(1, 1, 2, 'about', 'textarea', 'about', '', 'yes', '', '', '', ''),
(2, 1, 3, 'visi', 'textarea', 'visi', '', 'yes', '', '', '', ''),
(3, 1, 4, 'misi', 'editor_wysiwyg', 'misi', '', 'yes', '', '', '', ''),
(4, 2, 2, 'name', 'input', 'Name', '', 'yes', '', '', '', ''),
(5, 2, 3, 'email', 'email', 'Email', '', 'yes', '', '', '', ''),
(6, 2, 4, 'messsage', 'textarea', 'Messsage', '', 'yes', '', '', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_field_validation`
--

CREATE TABLE `form_field_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `form_field_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `form_field_validation`
--

INSERT INTO `form_field_validation` (`id`, `form_field_id`, `form_id`, `validation_name`, `validation_value`) VALUES
(1, 4, 2, 'required', ''),
(2, 4, 2, 'alpha', ''),
(3, 5, 2, 'required', ''),
(4, 5, 2, 'valid_email', ''),
(5, 6, 2, 'required', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `keys`
--

CREATE TABLE `keys` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL,
  `is_private_key` tinyint(1) NOT NULL,
  `ip_addresses` text DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, '17765ACFFEA76E728181532F96D83149', 0, 0, 0, NULL, '2020-08-27 21:57:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` int(11) UNSIGNED NOT NULL,
  `label` varchar(200) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `icon_color` varchar(200) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `sort` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `menu_type_id` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `label`, `type`, `icon_color`, `link`, `sort`, `parent`, `icon`, `menu_type_id`, `active`) VALUES
(1, 'MAIN NAVIGATION', 'label', '', '{admin_url}/dashboard', 1, 0, '', 1, 1),
(2, 'Dashboard', 'menu', '', '{admin_url}/dashboard', 2, 0, 'fa-dashboard', 1, 1),
(3, 'CRUD Builder', 'menu', '', '{admin_url}/crud', 11, 0, 'fa-table', 1, 1),
(4, 'API Builder', 'menu', '', '{admin_url}/rest', 7, 0, 'fa-code', 1, 1),
(5, 'Page Builder', 'menu', '', '{admin_url}/page', 8, 0, 'fa-file-o', 1, 1),
(6, 'Form Builder', 'menu', '', '{admin_url}/form', 10, 0, 'fa-newspaper-o', 1, 1),
(7, 'blog', 'menu', 'default', '{admin_url}/blog', 9, 0, 'fa-file-text-o', 1, 1),
(8, 'Menu', 'menu', '', '{admin_url}/menu', 12, 0, 'fa-bars', 1, 1),
(9, 'Auth', 'menu', '', '', 13, 0, 'fa-shield', 1, 1),
(10, 'User', 'menu', '', '{admin_url}/user', 14, 9, '', 1, 1),
(11, 'Groups', 'menu', '', '{admin_url}/group', 15, 9, '', 1, 1),
(12, 'Access', 'menu', '', '{admin_url}/access', 16, 9, '', 1, 1),
(13, 'Permission', 'menu', '', '{admin_url}/permission', 17, 9, '', 1, 1),
(14, 'API Keys', 'menu', '', '{admin_url}/keys', 18, 9, '', 1, 1),
(15, 'Extension', 'menu', '', '{admin_url}/extension', 19, 0, 'fa-puzzle-piece', 1, 1),
(16, 'OTHER', 'label', '', '', 20, 0, '', 1, 1),
(17, 'Settings', 'menu', 'text-red', '{admin_url}/setting', 21, 0, 'fa-circle-o', 1, 1),
(18, 'Web Documentation', 'menu', 'text-blue', '{admin_url}/doc/web', 22, 0, 'fa-circle-o', 1, 1),
(19, 'API Documentation', 'menu', 'text-yellow', '{admin_url}/doc/api', 23, 0, 'fa-circle-o', 1, 1),
(20, 'Home', 'menu', '', '/', 1, 0, '', 2, 1),
(21, 'Blog', 'menu', '', 'blog', 4, 0, '', 2, 1),
(22, 'Dashboard', 'menu', '', 'administrator/dashboard', 5, 0, '', 2, 1),
(23, 'About', 'menu', 'text-blue', '{admin_url}/about', 3, 0, 'fa-info', 1, 1),
(24, 'Products', 'menu', 'text-blue', '{admin_url}/products', 5, 0, 'fa-dropbox', 1, 1),
(25, 'Service', 'menu', 'text-blue', '{admin_url}/service', 4, 0, 'fa-bullhorn', 1, 1),
(27, 'Contact', 'menu', 'text-blue', '{admin_url}/contact', 6, 0, 'fa-envelope', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu_type`
--

CREATE TABLE `menu_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL,
  `definition` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `menu_type`
--

INSERT INTO `menu_type` (`id`, `name`, `definition`) VALUES
(1, 'side menu', NULL),
(2, 'top menu', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `page`
--

CREATE TABLE `page` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` varchar(200) NOT NULL,
  `type` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `fresh_content` text NOT NULL,
  `keyword` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `template` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `page`
--

INSERT INTO `page` (`id`, `title`, `type`, `content`, `fresh_content`, `keyword`, `description`, `link`, `template`, `created_at`) VALUES
(1, 'contact', 'frontend', '\n                            <cc-element cc-id=\"style\">\n                              \n                            </cc-element>\n\n                            <cc-element cc-id=\"content\">\n                              <div class=\"container ui-sortable\" style=\"\">\n                                <div class=\"row ui-sortable\" style=\"\">\n                                   <div class=\"column col-md-12 ui-sortable\" style=\"\">\n                                    <p style=\"\"><br></p>\n                                   </div>\n                                </div>\n                              </div>\n                            </cc-element>\n\n\n                            <cc-element cc-id=\"script\" cc-placement=\"top\">\n                               \n                            </cc-element><cc-element cc-id=\"style\">\n    <link data-src=\"stylesheet-bootstrap\" href=\"{base_url}cc-content/page-element/portofolio\\/package/vendor/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\n    <link data-src=\"stylesheet-freelancer\" href=\"{base_url}cc-content/page-element/portofolio\\/package/css/freelancer.min.css\" rel=\"stylesheet\">\n    <link data-src=\"stylesheet-font-awesome\" href=\"{base_url}cc-content/page-element/portofolio\\/package/vendor/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n    <link data-src=\"stylesheet-bootstrap\" href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\">\n    <link data-src=\"stylesheet-bootstrap\" href=\"https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">\n</cc-element>\n\n<cc-element cc-id=\"content\">\n     <section id=\"contact\" style=\"\" class=\"\">\n        <div class=\"container ui-sortable\" style=\"\">\n            <div class=\"row ui-sortable\" style=\"\">\n                <div class=\"col-lg-12 text-center ui-sortable\" style=\"\">\n                    <h2 style=\"\">Contact Me</h2>\n                    <hr class=\"star-primary\" style=\"\">\n                </div>\n            </div>\n            <div class=\"row ui-sortable\" style=\"\">\n                <div class=\"col-lg-8 col-lg-offset-2 ui-sortable\" style=\"\">\n                    <div class=\"form-builder ui-sortable\" style=\"\">{form_builder(2)}</div>\n                </div>\n            </div>\n        </div>\n    </section>\n</cc-element>\n\n\n<cc-element cc-id=\"script\" cc-placement=\"top\">\n    <script_widged class=\"display-none\" src=\"{base_url}cc-content/page-element/portofolio\\/package/vendor/bootstrap/js/bootstrap.min.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"{base_url}cc-content/page-element/portofolio\\/package/js/jqBootstrapValidation.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"{base_url}cc-content/page-element/portofolio\\/package/js/contact_me.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"{base_url}cc-content/page-element/portofolio\\/package/js/freelancer.min.js\"></script_widged>\n    <script_widged class=\"display-none\" type=\"text/javascript\">\n    </script_widged>\n</cc-element>', '\n                                    \n                                    \n                                    \n                                       <li class=\"block-item ui-draggable ui-draggable-handle block-item-loaded\" data-src=\"portofolio\\/column.php\" data-block-name=\"portofolio\\\" style=\"width: 200px; right: auto; height: 56px; bottom: auto; display: list-item;\"><div class=\"btn-wrapper pull-right ui-sortable\" style=\"margin-bottom:-10px; position:absolute; right:0%; z-index:99;\"><div class=\"btn btn-save-source btn-success btn-flat ui-sortable\"><i class=\"fa fa-check fa-icon-save\"></i></div><div class=\"btn btn-cancel-source btn-danger btn-flat ui-sortable\"><i class=\"fa fa-close fa-icon-close\"></i></div></div><pre class=\"source-code ace_editor ace-tm\" id=\"source-code\" style=\"width: 100%; resize: none; height: 221px;\"><textarea class=\"ace_text-input\" wrap=\"off\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\" style=\"opacity: 0; left: 143.369px; top: 52px; height: 14px; width: 6.59781px;\"></textarea><div class=\"ace_gutter ui-sortable\"><div class=\"ace_layer ace_gutter-layer ace_folding-enabled ui-sortable\" style=\"margin-top: -4px; height: 230px; width: 47px;\"><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">5</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">6</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">7</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">8</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">9<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">10<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">11<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">12<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">13<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">14</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">15</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">16</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">17</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">18<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">19<span class=\"ace_fold-widget ace_start ace_open\" style=\"height: 14px;\"></span></div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">20</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">21</div><div class=\"ace_gutter-cell  ui-sortable\" style=\"height: 14px;\">22</div></div><div class=\"ace_gutter-active-line ui-sortable\" style=\"top: 52px; height: 14px;\"></div></div><div class=\"ace_scroller ui-sortable\" style=\"left: 47px; right: 17px; bottom: 17px;\"><div class=\"ace_content ui-sortable\" style=\"margin-top: -4px; width: 1314px; height: 230px; margin-left: 0px;\"><div class=\"ace_layer ace_print-margin-layer ui-sortable\"><div class=\"ace_print-margin ui-sortable\" style=\"left: 531.825px; visibility: visible;\"></div></div><div class=\"ace_layer ace_marker-layer ui-sortable\"><div class=\"ace_active-line ui-sortable\" style=\"height:14px;top:56px;left:0;right:0;\"></div></div><div class=\"ace_layer ace_text-layer ui-sortable\" style=\"padding: 0px 4px;\"><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">link</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">data-src</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"stylesheet-bootstrap\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">href</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"https://fonts.googleapis.com/css?family=Montserrat:400,700\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">rel</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"stylesheet\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">type</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"text/css\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">link</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">data-src</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"stylesheet-bootstrap\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">href</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">rel</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"stylesheet\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">type</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"text/css\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_meta ace_tag ace_punctuation ace_end-tag-open ace_xml\">&lt;/</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">cc-element</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">cc-element</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">cc-id</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"content\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\"> </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">section</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">id</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"contact\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"container ui-sortable\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"row ui-sortable\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"col-lg-12 text-center ui-sortable\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">h2</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span><span class=\"ace_text ace_xml\">Contact Me</span><span class=\"ace_meta ace_tag ace_punctuation ace_end-tag-open ace_xml\">&lt;/</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">h2</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">hr</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"star-primary\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_end-tag-open ace_xml\">&lt;/</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_end-tag-open ace_xml\">&lt;/</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"row ui-sortable\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"col-lg-8 col-lg-offset-2 ui-sortable\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div><div class=\"ace_line ui-sortable\" style=\"height:14px\"><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_indent-guide\">    </span><span class=\"ace_text ace_xml\">    </span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-open ace_xml\">&lt;</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">class</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"form-builder ui-sortable\"</span><span class=\"ace_text ace_tag-whitespace ace_xml\"> </span><span class=\"ace_entity ace_other ace_attribute-name ace_xml\">style</span><span class=\"ace_keyword ace_operator ace_attribute-equals ace_xml\">=</span><span class=\"ace_string ace_attribute-value ace_xml\">\"\"</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span><span class=\"ace_text ace_xml\">{form_builder(2)}</span><span class=\"ace_meta ace_tag ace_punctuation ace_end-tag-open ace_xml\">&lt;/</span><span class=\"ace_meta ace_tag ace_tag-name ace_xml\">div</span><span class=\"ace_meta ace_tag ace_punctuation ace_tag-close ace_xml\">&gt;</span></div></div><div class=\"ace_layer ace_marker-layer ui-sortable\"></div><div class=\"ace_layer ace_cursor-layer ace_hidden-cursors ui-sortable\"><div class=\"ace_cursor ui-sortable\" style=\"left: 96.3694px; top: 56px; width: 6.59781px; height: 14px;\"></div></div></div></div><div class=\"ace_scrollbar ace_scrollbar-v ui-sortable\" style=\"width: 22px; bottom: 17px;\"><div class=\"ace_scrollbar-inner ui-sortable\" style=\"width: 22px; height: 504px;\"></div></div><div class=\"ace_scrollbar ace_scrollbar-h ui-sortable\" style=\"height: 22px; left: 47px; right: 17px;\"><div class=\"ace_scrollbar-inner ui-sortable\" style=\"height: 22px; width: 1314px;\"></div></div><div style=\"height: auto; width: auto; top: 0px; left: 0px; visibility: hidden; position: absolute; white-space: pre; font: inherit; overflow: hidden;\" class=\"ui-sortable\"><div style=\"height: auto; width: auto; top: 0px; left: 0px; visibility: hidden; position: absolute; white-space: pre; font: inherit; overflow: visible;\" class=\"ui-sortable\"></div><div style=\"height: auto; width: auto; top: 0px; left: 0px; visibility: hidden; position: absolute; white-space: pre; font-style: inherit; font-variant: inherit; font-stretch: inherit; font-size: inherit; line-height: inherit; font-family: inherit; overflow: visible;\" class=\"ui-sortable\">XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</div></div></pre>\n                        <div class=\"nav-content-wrapper noselect ui-sortable\" style=\"display: none;\">\n                          <i class=\"fa fa-gear\"></i>\n                          <div class=\"tool-nav delete ui-sortable\">\n                            <i class=\"fa fa-trash\"></i> <span class=\"info-nav\">Delete</span>\n                          </div>\n                          <div class=\"tool-nav source ui-sortable\">\n                            <i class=\"fa fa-code\"></i> <span class=\"info-nav\">Source</span>\n                          </div>\n                          <div class=\"tool-nav copy ui-sortable\">\n                            <i class=\"fa fa-copy\"></i> <span class=\"info-nav\">Copy</span>\n                          </div>\n                          <div class=\"tool-nav handle ui-sortable ui-sortable-handle\">\n                            <i class=\"fa fa-arrows\"></i> <span class=\"info-nav\">Move</span>\n                          </div>\n                        </div>\n                      \n                      <div class=\"block-content editable ui-sortable\" style=\"display: none;\">\n                            <cc-element cc-id=\"style\">\n                              \n                            </cc-element>\n\n                            <cc-element cc-id=\"content\">\n                              <div class=\"container ui-sortable\" style=\"\">\n                                <div class=\"row ui-sortable\" style=\"\">\n                                   <div class=\"column col-md-12 ui-sortable\" style=\"\">\n                                    <p style=\"\"><br></p>\n                                   </div>\n                                </div>\n                              </div>\n                            </cc-element>\n\n\n                            <cc-element cc-id=\"script\" cc-placement=\"top\">\n                               \n                            </cc-element></div>\n                    </li>\n                                    <li class=\"block-item ui-draggable ui-draggable-handle block-item-loaded\" data-src=\"portofolio\\/contact.php\" data-block-name=\"portofolio\\\" style=\"width: 0px; right: auto; height: 30px; bottom: auto; display: list-item;\"><div class=\"btn-wrapper pull-right ui-sortable\" style=\"margin-bottom:-10px; position:absolute; right:0%; z-index:99;\"><div class=\"btn btn-save-source btn-success btn-flat ui-sortable\"><i class=\"fa fa-check fa-icon-save\"></i></div><div class=\"btn btn-cancel-source btn-danger btn-flat ui-sortable\"><i class=\"fa fa-close fa-icon-close\"></i></div></div><pre class=\"source-code\" id=\"source-code\" style=\"width: 100%; resize: none; height: 221px;\"></pre>\n				                <div class=\"nav-content-wrapper noselect ui-sortable\" style=\"display: none;\">\n				                  <i class=\"fa fa-gear\"></i>\n				                  <div class=\"tool-nav delete ui-sortable\">\n				                    <i class=\"fa fa-trash\"></i> <span class=\"info-nav\">Delete</span>\n				                  </div>\n				                  <div class=\"tool-nav source ui-sortable\">\n				                    <i class=\"fa fa-code\"></i> <span class=\"info-nav\">Source</span>\n				                  </div>\n				                  <div class=\"tool-nav copy ui-sortable\">\n				                    <i class=\"fa fa-copy\"></i> <span class=\"info-nav\">Copy</span>\n				                  </div>\n				                  <div class=\"tool-nav handle ui-sortable ui-sortable-handle\">\n				                    <i class=\"fa fa-arrows\"></i> <span class=\"info-nav\">Move</span>\n				                  </div>\n				                </div>\n				              \n				              <div class=\"block-content editable ui-sortable\" style=\"display: none;\"><cc-element cc-id=\"style\">\n    <link data-src=\"stylesheet-bootstrap\" href=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/vendor/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\n    <link data-src=\"stylesheet-freelancer\" href=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/css/freelancer.min.css\" rel=\"stylesheet\">\n    <link data-src=\"stylesheet-font-awesome\" href=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/vendor/font-awesome/css/font-awesome.min.css\" rel=\"stylesheet\" type=\"text/css\">\n    <link data-src=\"stylesheet-bootstrap\" href=\"https://fonts.googleapis.com/css?family=Montserrat:400,700\" rel=\"stylesheet\" type=\"text/css\">\n    <link data-src=\"stylesheet-bootstrap\" href=\"https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic\" rel=\"stylesheet\" type=\"text/css\">\n</cc-element>\n\n<cc-element cc-id=\"content\">\n     <section id=\"contact\" style=\"\" class=\"\">\n        <div class=\"container ui-sortable\" style=\"\">\n            <div class=\"row ui-sortable\" style=\"\">\n                <div class=\"col-lg-12 text-center ui-sortable\" style=\"\">\n                    <h2 style=\"\">Contact Me</h2>\n                    <hr class=\"star-primary\" style=\"\">\n                </div>\n            </div>\n            <div class=\"row ui-sortable\" style=\"\">\n                <div class=\"col-lg-8 col-lg-offset-2 ui-sortable\" style=\"\">\n                    <div class=\"form-builder ui-sortable\" style=\"\">{form_builder(2)}</div>\n                </div>\n            </div>\n        </div>\n    </section>\n</cc-element>\n\n\n<cc-element cc-id=\"script\" cc-placement=\"top\">\n    <script_widged class=\"display-none\" src=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/vendor/bootstrap/js/bootstrap.min.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/js/jqBootstrapValidation.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/js/contact_me.js\"></script_widged>\n    <script_widged class=\"display-none\" src=\"http://localhost:80/ipm/cc-content/page-element/portofolio\\/package/js/freelancer.min.js\"></script_widged>\n    <script_widged class=\"display-none\" type=\"text/javascript\">\n    </script_widged>\n</cc-element></div>\n				            </li>                                                                                                            ', '', '', 'contact', 'default', '2020-09-02 17:13:28');

-- --------------------------------------------------------

--
-- Struktur dari tabel `page_block_element`
--

CREATE TABLE `page_block_element` (
  `id` int(11) UNSIGNED NOT NULL,
  `group_name` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image_preview` varchar(200) NOT NULL,
  `block_name` varchar(200) NOT NULL,
  `content_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `productID` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_image` varchar(255) NOT NULL,
  `product_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`productID`, `product_name`, `product_image`, `product_description`) VALUES
(1, 'box beauty', '20200831172026-2020-08-31products171839.webp', 'Elegant sekali'),
(2, 'box elegant', '20200831172137-2020-08-31products172117.webp', 'ramah sekali'),
(3, 'packaging', '20200901101300-2020-09-01products101245.webp', 'cek deskripsi'),
(4, 'packaging box', '20200901101618-2020-09-01products101553.jpg', 'packaging deskripsi'),
(5, 'products', '20200901101710-2020-09-01products101658.jpg', 'deskripsi product'),
(6, 'product Gs', '20200901101744-2020-09-01products101733.webp', 'deskripsi product');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rest`
--

CREATE TABLE `rest` (
  `id` int(11) UNSIGNED NOT NULL,
  `subject` varchar(200) NOT NULL,
  `table_name` varchar(200) NOT NULL,
  `primary_key` varchar(200) NOT NULL,
  `x_api_key` varchar(20) DEFAULT NULL,
  `x_token` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rest_field`
--

CREATE TABLE `rest_field` (
  `id` int(11) UNSIGNED NOT NULL,
  `rest_id` int(11) NOT NULL,
  `field_name` varchar(200) NOT NULL,
  `field_label` varchar(200) DEFAULT NULL,
  `input_type` varchar(200) NOT NULL,
  `show_column` varchar(10) DEFAULT NULL,
  `show_add_api` varchar(10) DEFAULT NULL,
  `show_update_api` varchar(10) DEFAULT NULL,
  `show_detail_api` varchar(10) DEFAULT NULL,
  `relation_table` varchar(200) DEFAULT NULL,
  `relation_value` varchar(200) DEFAULT NULL,
  `relation_label` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rest_field_validation`
--

CREATE TABLE `rest_field_validation` (
  `id` int(11) UNSIGNED NOT NULL,
  `rest_field_id` int(11) NOT NULL,
  `rest_id` int(11) NOT NULL,
  `validation_name` varchar(200) NOT NULL,
  `validation_value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rest_input_type`
--

CREATE TABLE `rest_input_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` varchar(200) NOT NULL,
  `relation` varchar(20) NOT NULL,
  `validation_group` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `rest_input_type`
--

INSERT INTO `rest_input_type` (`id`, `type`, `relation`, `validation_group`) VALUES
(1, 'input', '0', 'input'),
(2, 'timestamp', '0', 'timestamp'),
(3, 'file', '0', 'file'),
(4, 'select', '1', 'select');

-- --------------------------------------------------------

--
-- Struktur dari tabel `service`
--

CREATE TABLE `service` (
  `serviceID` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_image` varchar(255) NOT NULL,
  `service_description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `service`
--

INSERT INTO `service` (`serviceID`, `service_name`, `service_image`, `service_description`) VALUES
(1, 'Printing Machines', '20200901103858-2020-09-01service103856.jpg', 'We have 3 printing machines, one is 8 color and two is 9 color in order to produce good color quality. 8 Color Printing Produced by Orient Model OSG-K2 type VLS and the 9 Color Printing (Double Head) produced by Orient Model OSG-K2 type VLS.'),
(2, 'Extrusion Machines', '20200901104002-2020-09-01service103956.jpg', 'Besides the printing machines, we also have an extrusion machine and dry laminating machine to support the flexible packaging coating process. The purpose of this process is to coat plastic packaging using hot melt ore to the process of attachment of a base film with a coating film so that the ink and solvent printing results will not be contaminated by materials that are packed.'),
(3, 'Laboratory Equipment', '20200901104225-2020-09-01service104223.jpg', 'In the packaging world, we have to be aware that there is the aspect that can be measured and isn\'t that could be measured. With our laboratory equipment it\'s if the products is dasstabfield as good or not good (critical, major or minor). Low quality products are a burden for the company, and it\'s not good image for the company. We have several tools for checking the quality products in the form of critical criteria, major or minor in accordance the request of the customers.'),
(4, 'Slitting, Rewind Machine and Bag Making', '20200901104418-2020-09-01service104413.jpg', 'We used Slitting and Rewind machine to roll back the result of the machining process (laminating, printing and coating, etc.) with emphasis on the results of the roll as the quality of printing, neatness end of the roll, the density of the rolls, and reduce waste production efficiency and cutting jumbo rolls result of roll machine process be finished according to customer request. After the cutting process, we use a Bag Making Machine to make bags according to customers request.');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `aauth_groups`
--
ALTER TABLE `aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aauth_group_to_group`
--
ALTER TABLE `aauth_group_to_group`
  ADD PRIMARY KEY (`group_id`,`subgroup_id`);

--
-- Indeks untuk tabel `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aauth_perms`
--
ALTER TABLE `aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aauth_perm_to_user`
--
ALTER TABLE `aauth_perm_to_user`
  ADD PRIMARY KEY (`user_id`,`perm_id`);

--
-- Indeks untuk tabel `aauth_pms`
--
ALTER TABLE `aauth_pms`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aauth_user`
--
ALTER TABLE `aauth_user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aauth_users`
--
ALTER TABLE `aauth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `aauth_user_to_group`
--
ALTER TABLE `aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indeks untuk tabel `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `blog_category`
--
ALTER TABLE `blog_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indeks untuk tabel `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`);

--
-- Indeks untuk tabel `career`
--
ALTER TABLE `career`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `cc_options`
--
ALTER TABLE `cc_options`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud`
--
ALTER TABLE `crud`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_custom_option`
--
ALTER TABLE `crud_custom_option`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_field`
--
ALTER TABLE `crud_field`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_field_configuration`
--
ALTER TABLE `crud_field_configuration`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_field_validation`
--
ALTER TABLE `crud_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_input_chained`
--
ALTER TABLE `crud_input_chained`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_input_type`
--
ALTER TABLE `crud_input_type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `crud_input_validation`
--
ALTER TABLE `crud_input_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_about`
--
ALTER TABLE `form_about`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_contact`
--
ALTER TABLE `form_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_custom_attribute`
--
ALTER TABLE `form_custom_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_custom_option`
--
ALTER TABLE `form_custom_option`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_field`
--
ALTER TABLE `form_field`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_field_validation`
--
ALTER TABLE `form_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu_type`
--
ALTER TABLE `menu_type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `page_block_element`
--
ALTER TABLE `page_block_element`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productID`);

--
-- Indeks untuk tabel `rest`
--
ALTER TABLE `rest`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rest_field`
--
ALTER TABLE `rest_field`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rest_field_validation`
--
ALTER TABLE `rest_field_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `rest_input_type`
--
ALTER TABLE `rest_input_type`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`serviceID`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `aauth_groups`
--
ALTER TABLE `aauth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `aauth_perms`
--
ALTER TABLE `aauth_perms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT untuk tabel `aauth_pms`
--
ALTER TABLE `aauth_pms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `aauth_user`
--
ALTER TABLE `aauth_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `aauth_users`
--
ALTER TABLE `aauth_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `blog_category`
--
ALTER TABLE `blog_category`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=342;

--
-- AUTO_INCREMENT untuk tabel `career`
--
ALTER TABLE `career`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `cc_options`
--
ALTER TABLE `cc_options`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `crud`
--
ALTER TABLE `crud`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `crud_custom_option`
--
ALTER TABLE `crud_custom_option`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `crud_field`
--
ALTER TABLE `crud_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT untuk tabel `crud_field_configuration`
--
ALTER TABLE `crud_field_configuration`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `crud_field_validation`
--
ALTER TABLE `crud_field_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT untuk tabel `crud_input_chained`
--
ALTER TABLE `crud_input_chained`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `crud_input_type`
--
ALTER TABLE `crud_input_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `crud_input_validation`
--
ALTER TABLE `crud_input_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `form`
--
ALTER TABLE `form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `form_about`
--
ALTER TABLE `form_about`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `form_contact`
--
ALTER TABLE `form_contact`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `form_custom_attribute`
--
ALTER TABLE `form_custom_attribute`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `form_custom_option`
--
ALTER TABLE `form_custom_option`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `form_field`
--
ALTER TABLE `form_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `form_field_validation`
--
ALTER TABLE `form_field_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `menu_type`
--
ALTER TABLE `menu_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `page_block_element`
--
ALTER TABLE `page_block_element`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `rest`
--
ALTER TABLE `rest`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rest_field`
--
ALTER TABLE `rest_field`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rest_field_validation`
--
ALTER TABLE `rest_field_validation`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `rest_input_type`
--
ALTER TABLE `rest_input_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `service`
--
ALTER TABLE `service`
  MODIFY `serviceID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
