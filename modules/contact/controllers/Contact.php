<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *| --------------------------------------------------------------------------
 *| Blog Controller
 *| --------------------------------------------------------------------------
 *| For default controller
 *|
 */
class Contact extends Front
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_contact');
    }
    
    public function index()
    {
        $this->form_validation->set_rules('name', 'Your name', 'trim|required');
        $this->form_validation->set_rules('message', 'Send message', 'trim|required');
        $this->form_validation->set_rules('send', 'send', 'trim|required');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');
        
        $this->load->library('session');
        $this->form_validation->set_message('is_unique', 'User already used');
        if ($this->form_validation->run()) {
            $name = $this->input->post("name");
            $email = $this->input->post("email");
            $send = $this->input->post("send");
            $message = $this->input->post("message");
            $subject = $this->input->post("subject");
            $config = [
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'painempaijo724@gmail.com',
                'smtp_pass' => 'Rexfandi12345',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1'
            ];
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from($email, $name);
            $this->email->to($send);
            $this->email->subject($subject);
            $this->email->message($message);
            if ($this->email->send()) {
                $this->session->set_flashdata('message', 'Message Sent');
                redirect('/contact');
            } else {
                $this->session->set_flashdata('message', 'There is an error in email send');
                redirect('/contact');
            }
        } else {
            $data['error'] = validation_errors();
        }
        $this->template->build('contact/contact_index',$data);
    }
}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */