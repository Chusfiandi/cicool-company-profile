<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Contact Controller
*| --------------------------------------------------------------------------
*| Contact site
*|
*/
class Contact extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_contact');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Contacts
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('contact_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['contacts'] = $this->model_contact->get($filter, $field, $this->limit_page, $offset);
		$this->data['contact_counts'] = $this->model_contact->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/contact/index/',
			'total_rows'   => $this->data['contact_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Contact List');
		$this->render('backend/standart/administrator/contact/contact_list', $this->data);
	}
	
	/**
	* Add new contacts
	*
	*/
	public function add()
	{
		$this->is_allowed('contact_add');

		$this->template->title('Contact New');
		$this->render('backend/standart/administrator/contact/contact_add', $this->data);
	}

	/**
	* Add New Contacts
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('contact_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required');
		

		if ($this->form_validation->run()) {
		
			$save_data = [
				'fullname' => $this->input->post('fullname'),
				'email' => $this->input->post('email'),
				'jabatan' => $this->input->post('jabatan'),
			];

			
			$save_contact = $this->model_contact->store($save_data);
            

			if ($save_contact) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_contact;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/contact/edit/' . $save_contact, 'Edit Contact'),
						anchor('administrator/contact', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/contact/edit/' . $save_contact, 'Edit Contact')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/contact');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/contact');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Contacts
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('contact_update');

		$this->data['contact'] = $this->model_contact->find($id);

		$this->template->title('Contact Update');
		$this->render('backend/standart/administrator/contact/contact_update', $this->data);
	}

	/**
	* Update Contacts
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('contact_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('fullname', 'Fullname', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('jabatan', 'Jabatan', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'fullname' => $this->input->post('fullname'),
				'email' => $this->input->post('email'),
				'jabatan' => $this->input->post('jabatan'),
			];

			
			$save_contact = $this->model_contact->change($id, $save_data);

			if ($save_contact) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/contact', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/contact');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/contact');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Contacts
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('contact_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'contact'), 'success');
        } else {
            set_message(cclang('error_delete', 'contact'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Contacts
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('contact_view');

		$this->data['contact'] = $this->model_contact->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Contact Detail');
		$this->render('backend/standart/administrator/contact/contact_view', $this->data);
	}
	
	/**
	* delete Contacts
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$contact = $this->model_contact->find($id);

		
		
		return $this->model_contact->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('contact_export');

		$this->model_contact->export(
			'contact', 
			'contact',
			$this->model_contact->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('contact_export');

		$this->model_contact->pdf('contact', 'contact');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('contact_export');

		$table = $title = 'contact';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_contact->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file contact.php */
/* Location: ./application/controllers/administrator/Contact.php */