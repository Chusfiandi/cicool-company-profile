
<script src="<?= BASE_ASSET; ?>js/custom.js"></script>


<?= form_open('', [
    'name'    => 'form_form_about', 
    'class'   => 'form-horizontal form_form_about', 
    'id'      => 'form_form_about',
    'enctype' => 'multipart/form-data', 
    'method'  => 'POST'
]); ?>
 
<div class="form-group ">
    <label for="about" class="col-sm-2 control-label">About 
    </label>
    <div class="col-sm-8">
        <textarea id="about" name="about" rows="5" class="textarea form-control" ></textarea>
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="visi" class="col-sm-2 control-label">Visi 
    </label>
    <div class="col-sm-8">
        <textarea id="visi" name="visi" rows="5" class="textarea form-control" ></textarea>
        <small class="info help-block">
        </small>
    </div>
</div>
 
<div class="form-group ">
    <label for="misi" class="col-sm-2 control-label">Misi 
    </label>
    <div class="col-sm-8">
        <textarea id="misi" name="misi" rows="5" cols="80" ></textarea>
        <small class="info help-block">
        </small>
    </div>
</div>


<div class="row col-sm-12 message">
</div>
<div class="col-sm-2">
</div>
<div class="col-sm-8 padding-left-0">
    <button class="btn btn-flat btn-primary btn_save" id="btn_save" data-stype='stay'>
    Submit
    </button>
    <span class="loading loading-hide">
    <img src="http://localhost:80/ipm/asset//img/loading-spin-primary.svg"> 
    <i>Loading, Submitting data</i>
    </span>
</div>
</form></div>


<script src="<?= BASE_ASSET; ?>ckeditor/ckeditor.js"></script>
<!-- Page script -->
<script>
    $(document).ready(function(){
          $('.form-preview').submit(function(){
        return false;
     });

     $('input[type="checkbox"].flat-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
     });


    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
        $('#misi').val(misi.getData());
            
        var form_form_about = $('#form_form_about');
        var data_post = form_form_about.serializeArray();
        var save_type = $(this).attr('data-stype');
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + 'form/form_about/submit',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
            misi.setData(''); 
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 1000);
        });
    
        return false;
      }); /*end btn save*/


      
      CKEDITOR.replace('misi'); 
      var misi = CKEDITOR.instances.misi;
             
           
    }); /*end doc ready*/
</script>