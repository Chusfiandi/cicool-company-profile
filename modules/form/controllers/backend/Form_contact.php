<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Contact Controller
*| --------------------------------------------------------------------------
*| Form Contact site
*|
*/
class Form_contact extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_contact');
	}

	/**
	* show all Form Contacts
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('form_contact_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['form_contacts'] = $this->model_form_contact->get($filter, $field, $this->limit_page, $offset);
		$this->data['form_contact_counts'] = $this->model_form_contact->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/manage-form/form_contact/index/',
			'total_rows'   => $this->model_form_contact->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 5,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Contact List');
		$this->render('backend/standart/administrator/form_builder/form_contact/form_contact_list', $this->data);
	}

	/**
	* Update view Form Contacts
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('form_contact_update');

		$this->data['form_contact'] = $this->model_form_contact->find($id);

		$this->template->title('Contact Update');
		$this->render('backend/standart/administrator/form_builder/form_contact/form_contact_update', $this->data);
	}

	/**
	* Update Form Contacts
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('form_contact_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('name', 'Name', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('messsage', 'Messsage', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'messsage' => $this->input->post('messsage'),
			];

			
			$save_form_contact = $this->model_form_contact->change($id, $save_data);

			if ($save_form_contact) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/form_contact', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/form_contact');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					set_message('Your data not change.', 'error');
					
            		$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/form_contact');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	/**
	* delete Form Contacts
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('form_contact_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'Form Contact'), 'success');
        } else {
            set_message(cclang('error_delete', 'Form Contact'), 'error');
        }

		redirect_back();
	}

	/**
	* View view Form Contacts
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('form_contact_view');

		$this->data['form_contact'] = $this->model_form_contact->find($id);

		$this->template->title('Contact Detail');
		$this->render('backend/standart/administrator/form_builder/form_contact/form_contact_view', $this->data);
	}

	/**
	* delete Form Contacts
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$form_contact = $this->model_form_contact->find($id);

		
		return $this->model_form_contact->remove($id);
	}
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('form_contact_export');

		$this->model_form_contact->export('form_contact', 'form_contact');
	}
}


/* End of file form_contact.php */
/* Location: ./application/controllers/administrator/Form Contact.php */