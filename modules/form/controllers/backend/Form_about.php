<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form About Controller
*| --------------------------------------------------------------------------
*| Form About site
*|
*/
class Form_about extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_about');
	}

	/**
	* show all Form Abouts
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('form_about_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['form_abouts'] = $this->model_form_about->get($filter, $field, $this->limit_page, $offset);
		$this->data['form_about_counts'] = $this->model_form_about->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/manage-form/form_about/index/',
			'total_rows'   => $this->model_form_about->count_all($filter, $field),
			'per_page'     => $this->limit_page,
			'uri_segment'  => 5,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('About List');
		$this->render('backend/standart/administrator/form_builder/form_about/form_about_list', $this->data);
	}

	/**
	* Update view Form Abouts
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('form_about_update');

		$this->data['form_about'] = $this->model_form_about->find($id);

		$this->template->title('About Update');
		$this->render('backend/standart/administrator/form_builder/form_about/form_about_update', $this->data);
	}

	/**
	* Update Form Abouts
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('form_about_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'about' => $this->input->post('about'),
				'visi' => $this->input->post('visi'),
				'misi' => $this->input->post('misi'),
			];

			
			$save_form_about = $this->model_form_about->change($id, $save_data);

			if ($save_form_about) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/form_about', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/form_about');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
					set_message('Your data not change.', 'error');
					
            		$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/form_about');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	/**
	* delete Form Abouts
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('form_about_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'Form About'), 'success');
        } else {
            set_message(cclang('error_delete', 'Form About'), 'error');
        }

		redirect_back();
	}

	/**
	* View view Form Abouts
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('form_about_view');

		$this->data['form_about'] = $this->model_form_about->find($id);

		$this->template->title('About Detail');
		$this->render('backend/standart/administrator/form_builder/form_about/form_about_view', $this->data);
	}

	/**
	* delete Form Abouts
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$form_about = $this->model_form_about->find($id);

		
		return $this->model_form_about->remove($id);
	}
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('form_about_export');

		$this->model_form_about->export('form_about', 'form_about');
	}
}


/* End of file form_about.php */
/* Location: ./application/controllers/administrator/Form About.php */