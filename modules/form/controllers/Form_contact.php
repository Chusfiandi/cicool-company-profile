<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form Contact Controller
*| --------------------------------------------------------------------------
*| Form Contact site
*|
*/
class Form_contact extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_contact');
	}

	/**
	* Submit Form Contacts
	*
	*/
	public function submit()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required|alpha');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('messsage', 'Messsage', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'messsage' => $this->input->post('messsage'),
			];

			
			$save_form_contact = $this->model_form_contact->store($save_data);

			$this->data['success'] = true;
			$this->data['id'] 	   = $save_form_contact;
			$this->data['message'] = cclang('your_data_has_been_successfully_submitted');
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	
}


/* End of file form_contact.php */
/* Location: ./application/controllers/administrator/Form Contact.php */