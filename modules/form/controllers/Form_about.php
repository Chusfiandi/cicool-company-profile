<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Form About Controller
*| --------------------------------------------------------------------------
*| Form About site
*|
*/
class Form_about extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_form_about');
	}

	/**
	* Submit Form Abouts
	*
	*/
	public function submit()
	{
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'about' => $this->input->post('about'),
				'visi' => $this->input->post('visi'),
				'misi' => $this->input->post('misi'),
			];

			
			$save_form_about = $this->model_form_about->store($save_data);

			$this->data['success'] = true;
			$this->data['id'] 	   = $save_form_about;
			$this->data['message'] = cclang('your_data_has_been_successfully_submitted');
		} else {
			$this->data['success'] = false;
			$this->data['message'] = validation_errors();
		}

		echo json_encode($this->data);
	}

	
}


/* End of file form_about.php */
/* Location: ./application/controllers/administrator/Form About.php */