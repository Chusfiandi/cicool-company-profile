<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| About Controller
*| --------------------------------------------------------------------------
*| About site
*|
*/
class About extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_about');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Abouts
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('about_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['abouts'] = $this->model_about->get($filter, $field, $this->limit_page, $offset);
		$this->data['about_counts'] = $this->model_about->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/about/index/',
			'total_rows'   => $this->data['about_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('About List');
		$this->render('backend/standart/administrator/about/about_list', $this->data);
	}
	
	
		/**
	* Update view Abouts
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('about_update');

		$this->data['about'] = $this->model_about->find($id);

		$this->template->title('About Update');
		$this->render('backend/standart/administrator/about/about_update', $this->data);
	}

	/**
	* Update Abouts
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('about_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('about', 'About', 'trim|required|min_length[20]');
		$this->form_validation->set_rules('visi', 'Visi', 'trim|required|min_length[15]');
		$this->form_validation->set_rules('misi', 'Misi', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'about' => $this->input->post('about'),
				'visi' => $this->input->post('visi'),
				'misi' => $this->input->post('misi'),
			];

			
			$save_about = $this->model_about->change($id, $save_data);

			if ($save_about) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/about', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/about');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/about');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Abouts
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('about_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'about'), 'success');
        } else {
            set_message(cclang('error_delete', 'about'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Abouts
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('about_view');

		$this->data['about'] = $this->model_about->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('About Detail');
		$this->render('backend/standart/administrator/about/about_view', $this->data);
	}
	
	/**
	* delete Abouts
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$about = $this->model_about->find($id);

		
		
		return $this->model_about->remove($id);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('about_export');

		$this->model_about->export(
			'about', 
			'about',
			$this->model_about->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('about_export');

		$this->model_about->pdf('about', 'about');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('about_export');

		$table = $title = 'about';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_about->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file about.php */
/* Location: ./application/controllers/administrator/About.php */