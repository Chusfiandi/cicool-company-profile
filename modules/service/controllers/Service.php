<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
*| --------------------------------------------------------------------------
*| Blog Controller
*| --------------------------------------------------------------------------
*| For default controller
*|
*/
class Service extends Front
{
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('model_service');
	}

    public function index($offset = 0) 
    {
        $this->limit_page = 6;
        
        $filter = $this->input->get('q');
        $field  = $this->input->get('f');

        $this->data['service'] = $this->model_service->get($filter, $field, $this->limit_page, $offset);
        // $this->data['product_counts'] = $this->model_service->count_all($filter, $field);

        // $config = [
        //     'base_url'     => 'service/index/',
        //     'total_rows'   => $this->model_service->count_all($filter, $field),
        //     'per_page'     => $this->limit_page,
        //     'uri_segment'  => 3,
        // ];

        // $this->data['pagination'] = $this->pagination($config);

        $this->template->build('service/service_index', $this->data);
    }

    public function detail($productID = null) 
    {
        $product = $this->model_service->find_by_slug($productID);
        // $related = $this->model_product->get(null, null, 5, 0, $blog->category);

        $data = [
            'product' => $product,
            'product_name' => $product->product_name
        ];

        $this->template->build('product/product_read', $data);
    }
}


/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */