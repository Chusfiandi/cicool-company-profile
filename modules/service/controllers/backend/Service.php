<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/**
*| --------------------------------------------------------------------------
*| Service Controller
*| --------------------------------------------------------------------------
*| Service site
*|
*/
class Service extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('model_service');
		$this->lang->load('web_lang', $this->current_lang);
	}

	/**
	* show all Services
	*
	* @var $offset String
	*/
	public function index($offset = 0)
	{
		$this->is_allowed('service_list');

		$filter = $this->input->get('q');
		$field 	= $this->input->get('f');

		$this->data['services'] = $this->model_service->get($filter, $field, $this->limit_page, $offset);
		$this->data['service_counts'] = $this->model_service->count_all($filter, $field);

		$config = [
			'base_url'     => 'administrator/service/index/',
			'total_rows'   => $this->data['service_counts'],
			'per_page'     => $this->limit_page,
			'uri_segment'  => 4,
		];

		$this->data['pagination'] = $this->pagination($config);

		$this->template->title('Service List');
		$this->render('backend/standart/administrator/service/service_list', $this->data);
	}
	
	/**
	* Add new services
	*
	*/
	public function add()
	{
		$this->is_allowed('service_add');

		$this->template->title('Service New');
		$this->render('backend/standart/administrator/service/service_add', $this->data);
	}

	/**
	* Add New Services
	*
	* @return JSON
	*/
	public function add_save()
	{
		if (!$this->is_allowed('service_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('service_service_image_name', 'Service Image', 'trim|required');
		$this->form_validation->set_rules('service_description', 'Service Description', 'trim|required');
		

		if ($this->form_validation->run()) {
			$service_service_image_uuid = $this->input->post('service_service_image_uuid');
			$service_service_image_name = $this->input->post('service_service_image_name');
		
			$save_data = [
				'service_name' => $this->input->post('service_name'),
				'service_description' => $this->input->post('service_description'),
			];

			if (!is_dir(FCPATH . '/uploads/service/')) {
				mkdir(FCPATH . '/uploads/service/');
			}

			if (!empty($service_service_image_name)) {
				$service_service_image_name_copy = date('YmdHis') . '-' . $service_service_image_name;

				rename(FCPATH . 'uploads/tmp/' . $service_service_image_uuid . '/' . $service_service_image_name, 
						FCPATH . 'uploads/service/' . $service_service_image_name_copy);

				if (!is_file(FCPATH . '/uploads/service/' . $service_service_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['service_image'] = $service_service_image_name_copy;
			}
		
			
			$save_service = $this->model_service->store($save_data);
            

			if ($save_service) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $save_service;
					$this->data['message'] = cclang('success_save_data_stay', [
						anchor('administrator/service/edit/' . $save_service, 'Edit Service'),
						anchor('administrator/service', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_save_data_redirect', [
						anchor('administrator/service/edit/' . $save_service, 'Edit Service')
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/service');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/service');
				}
			}

		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
		/**
	* Update view Services
	*
	* @var $id String
	*/
	public function edit($id)
	{
		$this->is_allowed('service_update');

		$this->data['service'] = $this->model_service->find($id);

		$this->template->title('Service Update');
		$this->render('backend/standart/administrator/service/service_update', $this->data);
	}

	/**
	* Update Services
	*
	* @var $id String
	*/
	public function edit_save($id)
	{
		if (!$this->is_allowed('service_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}
		
		$this->form_validation->set_rules('service_name', 'Service Name', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('service_service_image_name', 'Service Image', 'trim|required');
		$this->form_validation->set_rules('service_description', 'Service Description', 'trim|required');
		
		if ($this->form_validation->run()) {
			$service_service_image_uuid = $this->input->post('service_service_image_uuid');
			$service_service_image_name = $this->input->post('service_service_image_name');
		
			$save_data = [
				'service_name' => $this->input->post('service_name'),
				'service_description' => $this->input->post('service_description'),
			];

			if (!is_dir(FCPATH . '/uploads/service/')) {
				mkdir(FCPATH . '/uploads/service/');
			}

			if (!empty($service_service_image_uuid)) {
				$service_service_image_name_copy = date('YmdHis') . '-' . $service_service_image_name;

				rename(FCPATH . 'uploads/tmp/' . $service_service_image_uuid . '/' . $service_service_image_name, 
						FCPATH . 'uploads/service/' . $service_service_image_name_copy);

				if (!is_file(FCPATH . '/uploads/service/' . $service_service_image_name_copy)) {
					echo json_encode([
						'success' => false,
						'message' => 'Error uploading file'
						]);
					exit;
				}

				$save_data['service_image'] = $service_service_image_name_copy;
			}
		
			
			$save_service = $this->model_service->change($id, $save_data);

			if ($save_service) {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = true;
					$this->data['id'] 	   = $id;
					$this->data['message'] = cclang('success_update_data_stay', [
						anchor('administrator/service', ' Go back to list')
					]);
				} else {
					set_message(
						cclang('success_update_data_redirect', [
					]), 'success');

            		$this->data['success'] = true;
					$this->data['redirect'] = base_url('administrator/service');
				}
			} else {
				if ($this->input->post('save_type') == 'stay') {
					$this->data['success'] = false;
					$this->data['message'] = cclang('data_not_change');
				} else {
            		$this->data['success'] = false;
            		$this->data['message'] = cclang('data_not_change');
					$this->data['redirect'] = base_url('administrator/service');
				}
			}
		} else {
			$this->data['success'] = false;
			$this->data['message'] = 'Opss validation failed';
			$this->data['errors'] = $this->form_validation->error_array();
		}

		echo json_encode($this->data);
	}
	
	/**
	* delete Services
	*
	* @var $id String
	*/
	public function delete($id = null)
	{
		$this->is_allowed('service_delete');

		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->_remove($id);
			}
		}

		if ($remove) {
            set_message(cclang('has_been_deleted', 'service'), 'success');
        } else {
            set_message(cclang('error_delete', 'service'), 'error');
        }

		redirect_back();
	}

		/**
	* View view Services
	*
	* @var $id String
	*/
	public function view($id)
	{
		$this->is_allowed('service_view');

		$this->data['service'] = $this->model_service->join_avaiable()->filter_avaiable()->find($id);

		$this->template->title('Service Detail');
		$this->render('backend/standart/administrator/service/service_view', $this->data);
	}
	
	/**
	* delete Services
	*
	* @var $id String
	*/
	private function _remove($id)
	{
		$service = $this->model_service->find($id);

		if (!empty($service->service_image)) {
			$path = FCPATH . '/uploads/service/' . $service->service_image;

			if (is_file($path)) {
				$delete_file = unlink($path);
			}
		}
		
		
		return $this->model_service->remove($id);
	}
	
	/**
	* Upload Image Service	* 
	* @return JSON
	*/
	public function upload_service_image_file()
	{
		if (!$this->is_allowed('service_add', false)) {
			echo json_encode([
				'success' => false,
				'message' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'service',
			'allowed_types' => 'jpg|png|jpeg',
			'max_size' 	 	=> 1500,
		]);
	}

	/**
	* Delete Image Service	* 
	* @return JSON
	*/
	public function delete_service_image_file($uuid)
	{
		if (!$this->is_allowed('service_delete', false)) {
			echo json_encode([
				'success' => false,
				'error' => cclang('sorry_you_do_not_have_permission_to_access')
				]);
			exit;
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => 'service_image', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'service',
            'primary_key'       => 'serviceID',
            'upload_path'       => 'uploads/service/'
        ]);
	}

	/**
	* Get Image Service	* 
	* @return JSON
	*/
	public function get_service_image_file($id)
	{
		if (!$this->is_allowed('service_update', false)) {
			echo json_encode([
				'success' => false,
				'message' => 'Image not loaded, you do not have permission to access'
				]);
			exit;
		}

		$service = $this->model_service->find($id);

		echo $this->get_file([
            'uuid'              => $id, 
            'delete_by'         => 'id', 
            'field_name'        => 'service_image', 
            'table_name'        => 'service',
            'primary_key'       => 'serviceID',
            'upload_path'       => 'uploads/service/',
            'delete_endpoint'   => 'administrator/service/delete_service_image_file'
        ]);
	}
	
	
	/**
	* Export to excel
	*
	* @return Files Excel .xls
	*/
	public function export()
	{
		$this->is_allowed('service_export');

		$this->model_service->export(
			'service', 
			'service',
			$this->model_service->field_search
		);
	}

	/**
	* Export to PDF
	*
	* @return Files PDF .pdf
	*/
	public function export_pdf()
	{
		$this->is_allowed('service_export');

		$this->model_service->pdf('service', 'service');
	}


	public function single_pdf($id = null)
	{
		$this->is_allowed('service_export');

		$table = $title = 'service';
		$this->load->library('HtmlPdf');
      
        $config = array(
            'orientation' => 'p',
            'format' => 'a4',
            'marges' => array(5, 5, 5, 5)
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->setDefaultFont('stsongstdlight'); 

        $result = $this->db->get($table);
       
        $data = $this->model_service->find($id);
        $fields = $result->list_fields();

        $content = $this->pdf->loadHtmlPdf('core_template/pdf/pdf_single', [
            'data' => $data,
            'fields' => $fields,
            'title' => $title
        ], TRUE);

        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output($table.'.pdf', 'H');
	}

	
}


/* End of file service.php */
/* Location: ./application/controllers/administrator/Service.php */