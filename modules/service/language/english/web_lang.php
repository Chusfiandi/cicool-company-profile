<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['service'] = 'Service';
$lang['serviceID'] = 'ServiceID';
$lang['service_name'] = 'Service Name';
$lang['service_image'] = 'Service Image';
$lang['service_description'] = 'Service Description';
