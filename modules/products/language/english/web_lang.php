<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['products'] = 'Products';
$lang['productID'] = 'ProductID';
$lang['product_name'] = 'Product Name';
$lang['product_image'] = 'Product Image';
$lang['product_description'] = 'Product Description';
$lang['qr_code'] = 'Qr Code';
