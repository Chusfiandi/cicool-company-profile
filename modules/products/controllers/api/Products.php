<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class Products extends API
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_api_products');
	}

	/**
	 * @api {get} /products/all Get all productss.
	 * @apiVersion 0.1.0
	 * @apiName AllProducts 
	 * @apiGroup products
	 * @apiHeader {String} X-Api-Key Productss unique access-key.
	 * @apiHeader {String} X-Token Productss unique token.
	 * @apiPermission Products Cant be Accessed permission name : api_products_all
	 *
	 * @apiParam {String} [Filter=null] Optional filter of Productss.
	 * @apiParam {String} [Field="All Field"] Optional field of Productss : productID, product_name, product_image, product_description.
	 * @apiParam {String} [Start=0] Optional start index of Productss.
	 * @apiParam {String} [Limit=10] Optional limit data of Productss.
	 *
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of products.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError NoDataProducts Products data is nothing.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function all_get()
	{
		$this->is_allowed('api_products_all');

		$filter = $this->get('filter');
		$field = $this->get('field');
		$limit = $this->get('limit') ? $this->get('limit') : $this->limit_page;
		$start = $this->get('start');

		$select_field = ['productID', 'product_name', 'product_image', 'product_description'];
		$productss = $this->model_api_products->get($filter, $field, $limit, $start, $select_field);
		$total = $this->model_api_products->count_all($filter, $field);
		$productss = array_map(function($row){
						
			return $row;
		}, $productss);

		$products_arr = [];

		foreach ($productss as $products) {
			$products->product_image  = BASE_URL.'uploads/products/'.$products->product_image;
			$products_arr[] = $products;
		}

		$data['products'] = $products_arr;
				
		$this->response([
			'status' 	=> true,
			'message' 	=> 'Data Products',
			'data'	 	=> $data,
			'total' 	=> $total,
		], API::HTTP_OK);
	}

		/**
	 * @api {get} /products/detail Detail Products.
	 * @apiVersion 0.1.0
	 * @apiName DetailProducts
	 * @apiGroup products
	 * @apiHeader {String} X-Api-Key Productss unique access-key.
	 * @apiHeader {String} X-Token Productss unique token.
	 * @apiPermission Products Cant be Accessed permission name : api_products_detail
	 *
	 * @apiParam {Integer} Id Mandatory id of Productss.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 * @apiSuccess {Array} Data data of products.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ProductsNotFound Products data is not found.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function detail_get()
	{
		$this->is_allowed('api_products_detail');

		$this->requiredInput(['productID']);

		$id = $this->get('productID');

		$select_field = ['productID', 'product_name', 'product_image', 'product_description'];
		$products = $this->model_api_products->find($id, $select_field);

		if (!$products) {
			$this->response([
					'status' 	=> false,
					'message' 	=> 'Blog not found'
				], API::HTTP_NOT_FOUND);
		}

					
		$data['products'] = $products;
		if ($data['products']) {
			$data['products']->product_image = BASE_URL.'uploads/products/'.$data['products']->product_image;
			
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Detail Products',
				'data'	 	=> $data
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Products not found'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	
	/**
	 * @api {post} /products/add Add Products.
	 * @apiVersion 0.1.0
	 * @apiName AddProducts
	 * @apiGroup products
	 * @apiHeader {String} X-Api-Key Productss unique access-key.
	 * @apiHeader {String} X-Token Productss unique token.
	 * @apiPermission Products Cant be Accessed permission name : api_products_add
	 *
 	 * @apiParam {String} Product_name Mandatory product_name of Productss.  
	 * @apiParam {File} Product_image Mandatory product_image of Productss. Extension file must JPG,PNG,JPEG. 
	 * @apiParam {String} Product_description Mandatory product_description of Productss.  
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function add_post()
	{
		$this->is_allowed('api_products_add');

		$this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('product_description', 'Product Description', 'trim|required');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'product_name' => $this->input->post('product_name'),
				'product_description' => $this->input->post('product_description'),
			];
			if (!is_dir(FCPATH . '/uploads/products')) {
				mkdir(FCPATH . '/uploads/products');
			}
			
			$config = [
				'upload_path' 	=> './uploads/products/',
				'allowed_types' => 'jpg|png|jpeg',
	'required' 		=> true
			];
			
			if ($upload = $this->upload_file('product_image', $config)){
				$upload_data = $this->upload->data();
				$save_data['product_image'] = $upload['file_name'];
			}

			$save_products = $this->model_api_products->store($save_data);

			if ($save_products) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully stored into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Validation Errors.',
				'errors' 	=> $this->form_validation->error_array()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}

	/**
	 * @api {post} /products/update Update Products.
	 * @apiVersion 0.1.0
	 * @apiName UpdateProducts
	 * @apiGroup products
	 * @apiHeader {String} X-Api-Key Productss unique access-key.
	 * @apiHeader {String} X-Token Productss unique token.
	 * @apiPermission Products Cant be Accessed permission name : api_products_update
	 *
	 * @apiParam {String} Product_name Mandatory product_name of Productss.  
	 * @apiParam {File} Product_image Mandatory product_image of Productss. Extension file must JPG,PNG,JPEG. 
	 * @apiParam {String} Product_description Mandatory product_description of Productss.  
	 * @apiParam {Integer} productID Mandatory productID of Products.
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function update_post()
	{
		$this->is_allowed('api_products_update');

		
		$this->form_validation->set_rules('product_name', 'Product Name', 'trim|required');
		$this->form_validation->set_rules('product_description', 'Product Description', 'trim|required');
		
		if ($this->form_validation->run()) {

			$save_data = [
				'product_name' => $this->input->post('product_name'),
				'product_description' => $this->input->post('product_description'),
			];
			if (!is_dir(FCPATH . '/uploads/products')) {
				mkdir(FCPATH . '/uploads/products');
			}
			
			$config = [
				'upload_path' 	=> './uploads/products/',
				'allowed_types' => 'jpg|png|jpeg',
	'required' 		=> true
			];
			
			if ($upload = $this->upload_file('product_image', $config)){
				$upload_data = $this->upload->data();
				$save_data['product_image'] = $upload['file_name'];
			}

			$save_products = $this->model_api_products->change($this->post('productID'), $save_data);

			if ($save_products) {
				$this->response([
					'status' 	=> true,
					'message' 	=> 'Your data has been successfully updated into the database'
				], API::HTTP_OK);

			} else {
				$this->response([
					'status' 	=> false,
					'message' 	=> cclang('data_not_change')
				], API::HTTP_NOT_ACCEPTABLE);
			}

		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Validation Errors.',
				'errors' 	=> $this->form_validation->error_array()
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
	/**
	 * @api {post} /products/delete Delete Products. 
	 * @apiVersion 0.1.0
	 * @apiName DeleteProducts
	 * @apiGroup products
	 * @apiHeader {String} X-Api-Key Productss unique access-key.
	 * @apiHeader {String} X-Token Productss unique token.
	 	 * @apiPermission Products Cant be Accessed permission name : api_products_delete
	 *
	 * @apiParam {Integer} Id Mandatory id of Productss .
	 *
	 * @apiSuccess {Boolean} Status status response api.
	 * @apiSuccess {String} Message message response api.
	 *
	 * @apiSuccessExample Success-Response:
	 *     HTTP/1.1 200 OK
	 *
	 * @apiError ValidationError Error validation.
	 *
	 * @apiErrorExample Error-Response:
	 *     HTTP/1.1 403 Not Acceptable
	 *
	 */
	public function delete_post()
	{
		$this->is_allowed('api_products_delete');

		$products = $this->model_api_products->find($this->post('productID'));

		if (!$products) {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Products not found'
			], API::HTTP_NOT_ACCEPTABLE);
		} else {
			$delete = $this->model_api_products->remove($this->post('productID'));

			if (!empty($products->product_image)) {
				$path = FCPATH . '/uploads/products/' . $products->product_image;

				if (is_file($path)) {
					$delete_file = unlink($path);
				}
			}

		}
		
		if ($delete) {
			$this->response([
				'status' 	=> true,
				'message' 	=> 'Products deleted',
			], API::HTTP_OK);
		} else {
			$this->response([
				'status' 	=> false,
				'message' 	=> 'Products not delete'
			], API::HTTP_NOT_ACCEPTABLE);
		}
	}
	
}

/* End of file Products.php */
/* Location: ./application/controllers/api/Products.php */