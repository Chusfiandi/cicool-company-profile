<?= get_header(); ?>

<body id="page-top">
    <?= get_navigation(); ?>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-lg-10 align-self-middle">
                    <h1 class="text-uppercase text-white font-weight-bold">Service
                    </h1>
                </div>
            </div>
        </div>
    </header>
    <section class="page-section">
    <?php foreach ($service as $s) : ?>
        <div class="container mt-5">
            <div class="row">
                <div class="col-md-7">
                    <h1><?= $s->service_name; ?></h1>
                    <hr>
                    <p><?= $s->service_description; ?></p>
                </div>
                <div class="col-md-5">
                    <div class="view overlay z-depth-1-half">
                        <img src="<?= BASE_URL . 'uploads/service/' . $s->service_image; ?>" class="card-img-top" alt="" style="height: 500px; width: 100%!important;">
                        <div class="mask rgba-white-light"></div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
    </section>

    <?= get_footer(); ?>