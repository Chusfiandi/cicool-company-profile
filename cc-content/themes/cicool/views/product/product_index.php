<?= get_header(); ?>

<body id="page-top">
    <?= get_navigation(); ?>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-lg-10 align-self-middle">
                    <h1 class="text-uppercase text-white font-weight-bold">Product
                    </h1>
                </div>
            </div>
        </div>
    </header>
    <section class="page-section">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-10 ">
                    <?php if ($q = $this->input->get('q')) : ?>
                        <div class="pull-left result-search"><b>Result for keyword "<?= $q ?>", found <span class="count-search-result"><?= $product_counts ?></span> Product</b></div>
                    <?php else : ?>
                        <div class="pull-left result-search"><b>Total : <span class="count-search-result"><?= $product_counts ?></span> Products</b></div>
                    <?php endif ?>
                    <div class="search-wrapper">

                        <form class="form-inline d-flex justify-content-center md-form form-sm active-cyan active-cyan-2 mt-2">
                            <input class="form-control form-control-sm ml-3 w-75" type="text" id="fsearch" name="q" placeholder="Search" value="<?= $this->input->get('q') ?>" aria-label="Search">
                            <i class="fas fa-search text-primary" aria-hidden="true"></i>
                        </form>
                    </div>
                    <div class="clearfix">

                    </div>
                </div>
            </div>
            <!-- Grid row -->
            <div class="row">
                <!-- Grid column -->
                <?php foreach ($products as $p) : ?>
                <div class="column-1 px-2 mb-r col-6 col-lg-4 col-md-6 mb-2">
                    <!--Card-->
                    <div class="card primary-color-dark">
                        <!--Card image-->
                        <div class="view">
                            <img src="<?= BASE_URL . 'uploads/products/' . $p->product_image; ?>" class="card-img-top"  alt="photo">
                            <a href="#">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <!--Card content-->
                        <div class="card-body text-center">
                            <!--Title-->
                            <h4 class="card-title white-text"><?= $p->product_name; ?></h4>
                            <!--Text-->
                            <p class="card-text white-text text-muted"><?= $p->product_description; ?></p>
                            <!-- <a href="#" class="btn btn-outline-white btn-md">Button</a> -->
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
        </div>
    </section>

    <?= get_footer(); ?>