<?= get_header(); ?>

<body id="page-top">
    <?= get_navigation(); ?>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-lg-10 align-self-middle">
                    <h1 class="text-uppercase text-white font-weight-bold">About
                    </h1>
                </div>
            </div>
        </div>
    </header>
    
        <?php foreach ($about as $a) : ?>
            <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <p><b class="text-primary">Basic</b> info about us</p>
                        <h1>PT. Intikemas Putra Makmur</h1>
                        <p><?= $a->about; ?></p>
                    </div>
                    <div class="col-md-5">
                        <div class="view overlay z-depth-1-half">
                            <img src="<?= theme_asset()  ?>/img/i.jpg" class="card-img-top" alt="" style=" width: 100%!important; height:100%;important">
                            <div class="mask rgba-white-light"></div>
                        </div>
                    </div>
                </div>
            </div>
            </section>
            <section class="page-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="view overlay z-depth-1-half">
                            <img src="<?= theme_asset()  ?>/img/IPM.jpeg" class="card-img-top" alt="" style=" width: 100%!important; height:100%;important">
                            <div class="mask rgba-white-light"></div>
                        </div>
                    </div>
                    <div class="col-md-7 pl-5">
                        <p class="text-primary">Visi & Misi</p>
                        <h1>Visi</h1>
                        <p><?= $a->visi; ?></p>
                        <h1 class="mt-3">Misi</h1>
                        <p><?= $a->misi; ?></p>
                    </div>
                </div>
            </div>
            </section>
        <?php endforeach ?>
    
    <?= get_footer(); ?>