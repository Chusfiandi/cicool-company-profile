<?= get_header();
$sendto = $this->db->get('contact')->result();
?>

<body id="page-top">
    <?= get_navigation() ?>
    <!-- Section: Contact v.1 -->
    <section class="page-section">

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5">Contact us</h2>
        <!-- Section description -->
        <p class="text-center w-responsive mx-auto pb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
            Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam
            eum porro a pariatur veniam.</p>
        <div class="container">
            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-lg-5 mb-lg-0 mb-4">

                    <!-- Form with header -->

                    <div class="card">
                        <div class="card-body">
                            <!-- Header -->
                            <div class="form-header blue">
                                <h3 class="mt-2 text-white text-center"><i class="fas fa-envelope"></i> Write to us:</h3>
                            </div>
                            <!-- Body -->
                            <?php
                            if ($this->session->flashdata("message")) {
                                echo "<div class='alert alert-success'>" . $this->session->flashdata("message") . "</div>";
                            }
                            ?>
                            <?php if (isset($error) and !empty($error)) : ?>
                                <div class="callout callout-error" style="color:#C82626">
                                    <h4><?= cclang('error'); ?>!</h4>
                                    <p><?= $error; ?></p>
                                </div>
                            <?php endif; ?>
                            <?= form_open('', [
                                'name'    => 'form_contact',
                                'id'      => 'form_contact',
                                'enctype' => 'multipart/form-data',
                                'method'  => 'POST'
                            ]); ?>
                            <div class="md-form">
                                <i class="fas fa-user prefix grey-text"></i>
                                <input type="text" id="form-name" class="form-control" name="name">
                                <label for="form-name">Your name</label>
                            </div>
                            <div class="md-form">
                                <i class="fas fa-envelope prefix grey-text"></i>
                                <input type="text" id="form-email" class="form-control" name="email">
                                <label for="form-email">Your email</label>
                            </div>
                            <div class="md-form">
                                <i class="fas fa-user-tie prefix grey-text"></i>
                                <select name="send" id="form-send" class="form-control" style="border:none;border-bottom:1px solid lightgrey;width:362px; margin-left:40px;">
                                    <option selected>Send to...</option>
                                    <?php foreach ($sendto as $s) : ?>
                                        <option value="<?= $s->email; ?>"><?= $s->jabatan; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="md-form">
                                <i class="fas fa-tag prefix grey-text"></i>
                                <input type="text" id="form-subject" class="form-control" name="subject">
                                <label for="form-subject">Subject</label>
                            </div>
                            <div class="md-form">
                                <i class="fas fa-pencil-alt prefix grey-text"></i>
                                <textarea id="form-message" class="form-control md-textarea" rows="3" name="message"></textarea>
                                <label for="form-message">Send message</label>
                            </div>
                            <?php $cap = get_captcha(); ?>
                            <div class="md-form <?= form_error('email') ? 'has-error' : ''; ?>">
                                <div class="captcha-box" data-captcha-time="<?= $cap['time']; ?>">
                                    <i class="fas fa-exclamation-circle prefix grey-text"></i>
                                    <label for="form-captha">Human Challenge</label>
                                    <input type="text" name="captcha" placeholder="" class="form-control" id="form-captha" style="width: 140px; position:absolute;">
                                    <a class="  refresh-captcha" style="margin-left:215px;margin-right:10px;"><i class="fas fa-sync-alt"></i></a>
                                    <span class="box-image"><?= $cap['image']; ?></span>
                                </div>
                            </div>
                            <small class="info help-block">
                            </small>
                            <div class="text-center">
                                <button class="btn btn-light-blue" type="submit">Send</button>
                            </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                    <!-- Form with header -->

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-7">

                    <!--Google map-->
                    <div id="map-container-section" class="z-depth-1-half map-container-section mb-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.342571454426!2d106.58206371475029!3d-6.2184761626376615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69fe8bb5b92701%3A0x8fe6f724edefd85a!2sPT%20Intikemas%20Putra%20Makmur!5e0!3m2!1sid!2sid!4v1599316776291!5m2!1sid!2sid" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <!-- Buttons-->
                    <div class="row text-center">
                        <div class="col-md-4">
                            <a class="btn btn-primary">
                                <i class="fas fa-map-marker-alt"></i>
                            </a>
                            <p>New York, 94126</p>
                            <p class="mb-md-0">United States</p>
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-primary">
                                <i class="fas fa-phone"></i>
                            </a>
                            <p>+ 01 234 567 89</p>
                            <p class="mb-md-0">Mon - Fri, 8:00-22:00</p>
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-primary">
                                <i class="fas fa-envelope"></i>
                            </a>
                            <p>info@gmail.com</p>
                            <p class="mb-0">sale@gmail.com</p>
                        </div>
                    </div>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->
        </div>
    </section>
    <!-- Section: Contact v.1 -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(function() {
            var BASE_URL = "<?= base_url(); ?>";

            $.fn.printMessage = function(opsi) {
                var opsi = $.extend({
                    type: 'success',
                    message: 'Success',
                    timeout: 500000
                }, opsi);

                $(this).hide();
                $(this).html(' <div class="col-md-12 message-alert" ><div class="callout callout-' + opsi.type + '"><h4>' + opsi.type + '!</h4>' + opsi.message + '</div></div>');
                $(this).slideDown('slow');
                // Run the effect
                setTimeout(function() {
                    $('.message-alert').slideUp('slow');
                }, opsi.timeout);
            };


            $('.refresh-captcha').on('click', function() {
                var capparent = $(this);

                $.ajax({
                        url: BASE_URL + '/captcha/reload/' + capparent.parent('.captcha-box').attr('data-captcha-time'),
                        dataType: 'JSON',
                    })
                    .done(function(res) {
                        capparent.parent('.captcha-box').find('.box-image').html(res.image);
                        capparent.parent('.captcha-box').attr('data-captcha-time', res.captcha.time);
                    })
                    .fail(function() {
                        $('.message').printMessage({
                            message: 'Error getting captcha',
                            type: 'warning'
                        });
                    })
                    .always(function() {});
            });
        });
    </script>
    <?= get_footer() ?>