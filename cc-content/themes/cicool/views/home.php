<?php
$about = $this->db->get('about')->result_array();
$service = $this->db->get('service')->result_array();
$jmlblog = $this->db->get('blog', 12)->num_rows();
$blog = $this->db->get('blog', 12)->result();

?>
<?= get_header() ?>

<body id="page-top">
    <?= get_navigation() ?>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-lg-8 align-self-end">
                    <p class="text-white font-weight-light">Ecommerce-ready Packaging
                    </p>
                </div>
                <div class="col-lg-10 align-self-baseline">
                    <h1 class="text-uppercase text-white font-weight-bold">flexible packaging
                    </h1>
                    <p class="text-white font-weight-light mb-5">Helping ensure your products survise whatever the supply chain has in store
                    </p>
                </div>
            </div>
        </div>
    </header>
    <!-- About-->
    <?php foreach ($about as $a) : ?>
        <section class="page-section" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <p><b class="text-primary">Basic</b> info about us</p>
                        <h1>PT. Intikemas Putra Makmur</h1>
                        <hr>
                        <p><?= $a['about']; ?></p>
                        <a href="<?= site_url('/about'); ?>" class="btn btn-indigo text-white">Selengkapnya</a>
                    </div>
                    <div class="col-md-5">
                        <div class="view overlay z-depth-1-half">
                            <img src="<?= theme_asset()  ?>/img/i.jpg" class="img-fluid" alt="">
                            <div class="mask rgba-white-light"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endforeach ?>
    <!-- Services-->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <h2 class="text-center mt-0">
                    <b class="text-primary">Our</b> Service
                </h2>
                <hr class="divider my-4" />
                <p class="text-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus
                    alias in aut
                    amet
                    deleniti itaque, ad debitis neque corporis. Cupiditate incidunt hic, expedita quis obcaecati ratione
                    reprehenderit aliquid dolorem fuga?</p>
            </div>
        </div>
        <div class="row">
            <?php foreach ($service as $s) : ?>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="mt-5">
                        <i class="fas fa-4x fa-gem text-primary mb-4"></i>
                        <h5 class=" mb-2"><?= $s['service_name']; ?></h5>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
    <section class="page-section">
        <div class="container-fluid">
            <div class="row justify-content-center mb-2">
                <div class="col-lg-8">
                    <h1 class="text-center">
                        <b>Latest News and Insight</b>
                    </h1>
                </div>
            </div>
        <div class="containera">
            <?php 
            $i=1;
            foreach($blog as $a): ?>
            <div class="box box<?= $i++ ?>"><img src="<?= BASE_URL . 'uploads/blog/' . $a->image; ?>" alt="">
            <div class="blog-title"><h5><?= $a->title; ?></h5></div></div>
            <?php endforeach; ?>
        </div>
    </section>
    
    <?= get_footer() ?>