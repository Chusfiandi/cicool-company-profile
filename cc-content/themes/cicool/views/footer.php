    <!-- Footer-->
    <!-- Footer -->
    <footer class="page-footer font-small primary-color-dark pt-4">

        <!-- Footer Links -->
        <div class="container-fluid text-center text-md-left">

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-6 mt-md-0 mt-3">

                    <!-- Content -->
                    <h5 class="text-uppercase">about us</h5>
                    <h6 class="text-uppercase">pt intikemas makmur putra</h6>
                    <p>is a company enggaged in the field of flexible packaging with 8-9 color printing system, the
                        latest extrusion machine and other systems to get best quality.</p>

                </div>
                <!-- Grid column -->

                <hr class="clearfix w-100 d-md-none pb-3">

                <!-- Grid column -->
                <div class="col-md-3 mb-md-0 mb-3">

                    <!-- Links -->
                    <h5 class="text-uppercase"><a href="<?= site_url('/service') ?>">service</a></h5>

                    <ul class="list-unstyled">
                        <li>
                            <p>Printing Machine</p>
                        </li>
                        <li>
                            <p>Extrusion & Dry Machine</p>
                        </li>
                        <li>
                            <p>Sliting, Rewind And Bag Making</p>
                        </li>
                        <li>
                            <p>Laboratory Equiment</p>
                        </li>
                    </ul>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-3 mb-md-0 mb-3">

                    <!-- Links -->
                    <h5 class="text-uppercase">Contact us</h5>

                    <ul class="list-unstyled">
                        <li>
                            <a href="#!">(021)55653637</a>
                        </li>
                        <li>
                            <a href="#!">(021)55653706</a>
                        </li>
                        <li>
                            <a href="#!">hrd@intikemaspm.com</a>
                        </li>
                    </ul>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

        </div>
        <!-- Footer Links -->

        <!-- Copyright -->
        <div class="footer-copyright text-center elegant-color-dark py-3">© 2020 Copyright:
            <a href="https://majapahit.id/" target="_blank"> Majapahit Teknologi</a>
        </div>
        <!-- Copyright -->

    </footer>
    <!-- Footer -->
    <!-- Bootstrap core JS-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Core theme JS-->
    <script src="<?= theme_asset(); ?>/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="<?= theme_asset(); ?>/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.0/js/mdb.min.js"></script>
    </body>

    </html>