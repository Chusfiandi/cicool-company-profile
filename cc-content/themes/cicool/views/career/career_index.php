<?= get_header(); ?>

<body id="page-top">
    <?= get_navigation(); ?>
    <!-- Masthead-->
    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-lg-10 align-self-middle">
                    <h1 class="text-uppercase text-white font-weight-bold">Career
                    </h1>
                </div>
            </div>
        </div>
    </header>

    <section class="page-section">
        <div class="container">
                <h1 class="text-primary">Career</h1>
                <hr>
                <?php foreach ($career as $c) : ?>
                <p class="text-primary"><?= $c->title; ?></p>
                <hr>
                    <?= $c->content; ?>
        </div>
    </section>
<?php endforeach ?>

<?= get_footer(); ?>