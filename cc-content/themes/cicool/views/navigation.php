<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="#page-top"><img src="<?= theme_asset(); ?>/img/IPM.jpeg" width="60" height="40" alt="" loading="lazy"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto my-2 my-lg-0 ">
                <li class="nav-item"><a class=" nav-link <?php if($this->uri->segment(1) == ''){echo 'active';} ?>" href="<?= site_url('/'); ?>">Home page</a></li>
                <li class="nav-item"><a class=" nav-link <?php if($this->uri->segment(1) == 'about'){echo 'active';} ?>" href="<?= site_url('/about'); ?>">About us</a></li>
                <li class="nav-item"><a class=" nav-link <?php if($this->uri->segment(1) == 'service'){echo 'active';} ?>" href="<?= site_url('/service'); ?>">Services</a></li>
                <li class="nav-item"><a class=" nav-link <?php if($this->uri->segment(1) == 'products'){echo 'active';} ?>" href="<?= site_url('/products'); ?>">Product</a></li>
                <li class="nav-item"><a class=" nav-link <?php if($this->uri->segment(1) == 'career'){echo 'active';} ?>" href="<?= site_url('/career'); ?>">Career</a></li>
                <li class="nav-item"><a class=" nav-link <?php if($this->uri->segment(1) == 'contact'){echo 'active';}?>" href="<?= site_url('/contact'); ?>">Contact</a></li>
                <li></li>
            </ul>
        </div>
    </div>
</nav>